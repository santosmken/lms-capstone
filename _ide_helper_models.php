<?php
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Models,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\Assignment
 *
 * @property integer $id
 * @property string $title
 * @property string $due_date
 * @property string $due_time
 * @property string $body
 * @property integer $lock
 * @property string $file_name
 * @property string $old_file_name
 * @property integer $user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\User $user
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Group[] $groups
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Turnin[] $turnins
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Comment[] $comments
 * @method static \Illuminate\Database\Query\Builder|\App\Assignment whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Assignment whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Assignment whereDueDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Assignment whereDueTime($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Assignment whereBody($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Assignment whereLock($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Assignment whereFileName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Assignment whereOldFileName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Assignment whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Assignment whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Assignment whereUpdatedAt($value)
 */
	class Assignment extends \Eloquent {}
}

namespace App{
/**
 * App\Comment
 *
 * @property integer $id
 * @property string $comment
 * @property integer $post_id
 * @property integer $assignment_id
 * @property integer $user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\User $user
 * @property-read \App\Post $post
 * @property-read \App\Assignment $assignment
 * @property-read \App\Turnin $turnin
 * @method static \Illuminate\Database\Query\Builder|\App\Comment whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Comment whereComment($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Comment wherePostId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Comment whereAssignmentId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Comment whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Comment whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Comment whereUpdatedAt($value)
 */
	class Comment extends \Eloquent {}
}

namespace App{
/**
 * App\File
 *
 * @property integer $id
 * @property string $file_name
 * @property string $file_size
 * @property integer $user_id
 * @property integer $folder_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\User $user
 * @property-read \App\Folder $folder
 * @method static \Illuminate\Database\Query\Builder|\App\File whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\File whereFileName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\File whereFileSize($value)
 * @method static \Illuminate\Database\Query\Builder|\App\File whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\File whereFolderId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\File whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\File whereUpdatedAt($value)
 */
	class File extends \Eloquent {}
}

namespace App{
/**
 * App\Folder
 *
 * @property integer $id
 * @property string $folder_title
 * @property integer $group_id
 * @property integer $user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\User $user
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\File[] $files
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Group[] $group
 * @method static \Illuminate\Database\Query\Builder|\App\Folder whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Folder whereFolderTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Folder whereGroupId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Folder whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Folder whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Folder whereUpdatedAt($value)
 */
	class Folder extends \Eloquent {}
}

namespace App{
/**
 * App\Group
 *
 * @property integer $id
 * @property string $group_name
 * @property string $college_year
 * @property string $section
 * @property string $subject
 * @property string $group_code
 * @property integer $user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @property-read \App\User $user
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Post[] $posts
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Assignment[] $assignments
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Folder[] $folders
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Quiz[] $quizzes
 * @method static \Illuminate\Database\Query\Builder|\App\Group whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Group whereGroupName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Group whereCollegeYear($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Group whereSection($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Group whereSubject($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Group whereGroupCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Group whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Group whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Group whereUpdatedAt($value)
 */
	class Group extends \Eloquent {}
}

namespace App{
/**
 * App\Notes
 *
 * @property integer $id
 * @property string $note
 * @property integer $user_id
 * @property integer $grade
 * @property integer $total
 * @property integer $turnin_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\User $user
 * @property-read \App\Turnin $turnin
 * @method static \Illuminate\Database\Query\Builder|\App\Notes whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Notes whereNote($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Notes whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Notes whereGrade($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Notes whereTotal($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Notes whereTurninId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Notes whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Notes whereUpdatedAt($value)
 */
	class Notes extends \Eloquent {}
}

namespace App{
/**
 * App\Post
 *
 * @property integer $id
 * @property string $body
 * @property integer $user_id
 * @property string $published_at
 * @property string $published_time
 * @property string $file_name
 * @property string $old_file_name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\User $user
 * @property-read \App\Group $group
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Group[] $groups
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Comment[] $comments
 * @method static \Illuminate\Database\Query\Builder|\App\Post whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Post whereBody($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Post whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Post wherePublishedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Post wherePublishedTime($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Post whereFileName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Post whereOldFileName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Post whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Post whereUpdatedAt($value)
 */
	class Post extends \Eloquent {}
}

namespace App{
/**
 * App\Question
 *
 * @property integer $id
 * @property string $question
 * @property string $first_answer
 * @property string $second_answer
 * @property string $third_answer
 * @property string $fourth_answer
 * @property boolean $correct_answer
 * @property integer $user_id
 * @property integer $quiz_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Quiz $quiz
 * @method static \Illuminate\Database\Query\Builder|\App\Question whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Question whereQuestion($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Question whereFirstAnswer($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Question whereSecondAnswer($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Question whereThirdAnswer($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Question whereFourthAnswer($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Question whereCorrectAnswer($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Question whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Question whereQuizId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Question whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Question whereUpdatedAt($value)
 */
	class Question extends \Eloquent {}
}

namespace App{
/**
 * App\Quiz
 *
 * @property integer $id
 * @property string $title
 * @property string $time_limit
 * @property string $type
 * @property string $description
 * @property integer $user_id
 * @property integer $group_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\User $user
 * @property-read \App\Group $group
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Group[] $groups
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Question[] $questions
 * @method static \Illuminate\Database\Query\Builder|\App\Quiz whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Quiz whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Quiz whereTimeLimit($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Quiz whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Quiz whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Quiz whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Quiz whereGroupId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Quiz whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Quiz whereUpdatedAt($value)
 */
	class Quiz extends \Eloquent {}
}

namespace App{
/**
 * App\Turnin
 *
 * @property integer $id
 * @property string $description
 * @property string $filename
 * @property string $previous_filename
 * @property integer $grade
 * @property integer $total
 * @property integer $assignment_id
 * @property integer $user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\User $user
 * @property-read \App\Assignment $assignment
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Notes[] $notes
 * @method static \Illuminate\Database\Query\Builder|\App\Turnin whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Turnin whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Turnin whereFilename($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Turnin wherePreviousFilename($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Turnin whereGrade($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Turnin whereTotal($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Turnin whereAssignmentId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Turnin whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Turnin whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Turnin whereUpdatedAt($value)
 */
	class Turnin extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $avatar
 * @property string $password
 * @property string $role
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Group[] $groups
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Post[] $posts
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Assignment[] $assignments
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Folder[] $folders
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\File[] $files
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Turnin[] $turnins
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Comment[] $comments
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Notes[] $notes
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Quiz[] $quizzes
 * @method static \Illuminate\Database\Query\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereFirstName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereLastName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereAvatar($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereRole($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

