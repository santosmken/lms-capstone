<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    //
    protected $fillable = [
        'title',
        'body',
        'due_date',
        'due_time',
        'lock',
        
    ];

        public function user()
    {
        return $this->belongsTo(User::class);
    }

        public function groups()
    {
        return $this->belongsToMany(Group::class);
    }

        public function turnins()
    {
        return $this->hasMany(Turnin::class);
    }

    public function hasTurnInFromUser($userId)
    {
        return $this->turnins()->where('user_id', $userId)->count() > 0;
    }

    public function hasNotTurnInFromUser($userId)
    {
        return $this->turnins()->where('user_id', $userId)->count() < 0;
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }


}
