<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function assignment()
    {
        return $this->belongsTo(Assignment::class);
    }

    public function turnin()
    {
        return $this->belongsTo(Turnin::class);
    }
}
