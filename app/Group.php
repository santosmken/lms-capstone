<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    public function users()
    {
        return $this->belongsToMany(User::class, 'group_user')->withTimestamps();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }

    public function assignments()
    {
        return $this->belongsToMany(Assignment::class)->latest();
    }

    public function folders()
    {
        return $this->hasMany(Folder::class);
    }

    public function quizzes()
    {
        return $this->hasMany(Quiz::class);
    }


}
