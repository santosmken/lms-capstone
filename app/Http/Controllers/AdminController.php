<?php

namespace App\Http\Controllers;

use App\Group;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function addUser(Request $request)
    {
        $this->validate($request, [
           'first_name' => 'required|regex:/^[\pL\s\-]+$/u',
            'last_name' => 'required|regex:/^[\pL\s\-]+$/u',
            'email' => 'unique:users,email'
        ]);

        $user = new User();
        $user->first_name = $request['first_name'];
        $user->last_name = $request['last_name'];
        $user->email     = $request['email'];
        $user->password = bcrypt('sample');
        $user->save();

        return back();
    }

    public function allUsers()
    {
        $user = Auth::user();
        $users = User::all();

        return view('admin.all_users')
            ->withUsers($users)
            ->withUser($user);
    }

    public function edit(User $user)
    {
        return view('admin.user_details', compact('user'));
    }

    public function update(Request $request)
    {

    }

    public function deleteUser(User $user)
    {
        $user->delete();
        return back();
    }
}
