<?php

namespace App\Http\Controllers;

use App\Assignment;
use App\Group;
use App\Turnin;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class AssignmentController extends Controller
{
    public function assignment(Request $request, Group $group)
    {
        $groups = Group::all();
        $user = Auth::user()->id;

        $this->validate($request ,[
            'title' => 'required',
            'body'  => 'required|max:120',
        ]);

        $assignment = new Assignment();
        $assignment->title = $request['title'];
        $assignment->body  = $request['body'];
        $assignment->due_date = $request['due_date'];
        $assignment->due_time = $request['due_time'];
        $assignment->lock = $request['lock'];
        $assignment->user_id = $user;

        if ($request->hasFile('file'))
        {
            $file = $request->file('file');
            $filename = time() . '.' .$file->getClientOriginalExtension();
            $originalname = $file->getClientOriginalName();
            $location = public_path('attachments/');
            $file->move($location, $file->getClientOriginalName());
            $assignment->file_name = $filename;
            $assignment->old_file_name = $originalname;
        }

        $assignment->save();
        $assignment->groups()->sync($request->groups, false);

        Session::flash('success', 'Assignment successfully created!');

        return redirect()->back()
            ->withGroups($groups)
            ->withUser(Auth::user());
    }

    public function create(Request $request, Group $group)
    {
        $user = Auth::user()->id;

        $this->validate($request, [
            'title'    => 'required|max:120',
            'body'     => 'required|max:255',
            'due_date' => 'required',
            'due_time' => 'required',
        ]);

        $assignment = new Assignment();
        $assignment->title = $request['title'];
        $assignment->body  = $request['body'];
        $assignment->due_date  = $request['due_date'];
        $assignment->due_time  = $request['due_time'];
        $assignment->lock  = $request['lock'];
        $assignment->user_id = $user;

        if ($request->hasFile('file'))
        {
            $file = $request->file('file');
            $filename = time() . '.' .$file->getClientOriginalExtension();
            $originalname = $file->getClientOriginalName();
            $location = public_path('attachments/');
            $file->move($location, $file->getClientOriginalName());
            $assignment->file_name = $filename;
            $assignment->old_file_name = $originalname;
        }
        $group->assignments()->save($assignment);

        return redirect()->back()
            ->withGroup($group)
            ->withUser(Auth::user());
    }

    public function edit($assignment_id)
    {
        $assignment = Assignment::find($assignment_id);
        return back(compact('assignment'));
    }

    public function update(Request $request, $assignment_id)
    {
        $assignment = Assignment::find($assignment_id);
        $assignment->title = $request->title;
        $assignment->body = $request->body;
        $assignment->due_date = $request->due_date;
        $assignment->due_time = $request->due_time;
        $assignment->save();

        Session::flash('succcess', 'Assignment successfully updated!');
        return back();
    }

    public function getDeleteAssignment($assignment_id)
    {
        $assignment = Assignment::where('id', $assignment_id)->first();
        if (Auth::user() != $assignment->user){
            return back();
        }
        $assignment->delete();
        Session::flash('success', 'Successfully Delete!');
        return back();
    }

    public function show($group, $id)
    {
        $user = Auth::user();
        $assignment = Assignment::find($id);
        $turnins = Turnin::where('assignment_id', $assignment->id)->get();
        $turnin = Turnin::with('group')->get();
        $users = User::all();
        return view('group.assignment_show', compact(
            'turnin',
            'turnins',
            'assignment',
            'users',
            'user'
        ));
    }

    public function studentSubmitted($group, $id)
    {
//        $user = User::all();
//        $assignment = Assignment::find($assignment_id);
//        $turnin = Turnin::find($user_id);
//
//        return view('group.assignment_grading')
////            ->withUser(Auth::user())
//                ->withassignment($assignment)
//                ->withUser($user)
//            ->withturnin($turnin);
        $user = Auth::user();
        $assignment = Assignment::find($id);
        $turnins = Turnin::where('assignment_id', $assignment->id)->get();
        $users = User::all();
        return view('group.assignment_show', compact(
            'turnins',
            'assignment',
            'users',
            'user'
        ));
    }

}
