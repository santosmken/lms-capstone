<?php

namespace App\Http\Controllers;

use App\Assignment;
use App\Comment;
use App\Post;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function create(Request $request, Post $post_id)
    {
        $this->validate($request, [
           'comment' => 'max:120|min:3'
        ]);

        $comment = new Comment();
        $comment->comment = $request['comment'];
        $comment->user_id = Auth::user()->id;
        $post_id->comments()->save($comment);
        return redirect()->back();
    }

    public function commentAssignment(Request $request, Assignment $assignment_id)
    {
        $this->validate($request, [
            'comment' => 'max:120|min:3'
        ]);

        $comment = new Comment();
        $comment->comment = $request['comment'];
        $comment->user_id = Auth::user()->id;
        $assignment_id->comments()->save($comment);
        return redirect()->back();
    }
}
