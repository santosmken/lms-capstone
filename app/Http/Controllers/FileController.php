<?php

namespace App\Http\Controllers;

use App\Folder;
use App\Post;
use App\Turnin;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use App\File;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $files = File::all();
        $folders = Folder::all();
//        $directories = Storage::directories($folders);
        return view('locker.personal_locker')
            ->withUser($user)
            ->withFolders($folders);
//            ->withFiles($files);
    }

    public function store(Request $request)
    {
        $user = Auth::user()->id;
        $upload = new File();
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $destination = storage_path('locker/');
            $file->move($destination, $file->getClientOriginalName());
            $filesize = $file->getClientSize();
            $upload->user_id = $user;
            $upload->file_name = $file->getClientOriginalName();
            //CHANGE saving type time() . getoriginalname
        }
        $upload->save();
        return redirect()->back();
    }

    public function download($filename)
    {
        $entry = File::where('file_name', '=', $filename)->firstOrFail();
        $file = Storage::disk('private')->get($entry->file_name);

        return(new Response($file, 200))
            ->header('Content-Type', $entry->mime);

//        return response()->download($file, $entry);
    }

    public function createFolder(Request $request)
    {
        $user = Auth::user()->id;

        $this->validate($request ,[
            'folder_title' => 'required|min:3|max:30',
        ]);

        $folder = new Folder();
        $folder->folder_title = $request['folder_title'];
        $foldername = $folder->folder_title;
        $permission = intval(0775);
        Storage::disk('private')->makeDirectory($foldername, $permission, true);
        $folder->user_id = $user;
        $folder->save();

        return redirect()->back();
    }

    public function deleteFile(File $file)
    {
        $file_path = storage_path('locker').'/'. $file->file_name;
        $file->delete();
        unlink($file_path);
        return back();
    }

    public function deleteFolder(Folder $folder)
    {
        $folder_path = storage_path('locker').'/'. $folder->folder_title;
        $folder->delete();
        rmdir($folder_path);
        return back();
    }

    public function getFolder($folder_id)
    {
        $folder = Folder::find($folder_id);
        $user = Auth::user();
        return view('locker.folder')
            ->withFolder($folder)
            ->withUser($user);
    }

    public function addFilesToFolder(Request $request, $folder_id)
    {
        $folder = Folder::find($folder_id);
        $file = new File();
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $destination = storage_path('locker/') . $folder->folder_title . '/';
            $file->move($destination, $file->getClientOriginalName());
            $file->user_id = Auth::user()->id;
            $file->file_name = $file->getClientOriginalName();
        }

        $file->save();
        $folder->files()->attach($file);
        return back();
    }

    public function downloadFile(File $file_id)
    {
        $path = storage_path('locker/') . $file_id->file_name ;
        return response()->download($path);
    }

    public function downloadGroupFile(Post $post_id)
    {
        $path = public_path('attachments/') . $post_id->file_name;
        return response()->download($path);
    }

    public function downloadTurnedInFile(Turnin $turnin_id)
    {
        $path = public_path('attachments/') . $turnin_id->previous_filename;
        return response()->download($path);
    }


    public function groupFolder(Folder $folder)
    {
        return 'Okay';
    }
}