<?php

namespace App\Http\Controllers;

use App\Assignment;
use App\Group;
use App\Notes;
use App\Turnin;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class GradingController extends Controller
{
    public function grade(Request $request, Turnin $turnin)
    {
//        $assignment = Assignment::find($assignment);
        $turnins = Turnin::with('user', 'assignment')->get();
        $assignment = Assignment::with('turnin', 'users');
        $users = User::all();
        $user = Auth::user();
        $notes = Notes::with('turnins');
//        $turnin= Turnin::find($id);

//        $turnin = Assignment::with('turnins.user')->find(10);
//        return $turnin;
//        $uid->load('turnins.assignment');
//        return $uid;
//        $group = Group::all();
//        $user = Auth::user();
        return view('group.assignment_grading')
            ->withassignment($assignment)
            ->withturnins($turnins)
            ->withusers($users)
            ->withNotes($notes)
//            ->withGroup($group)
            ->withturnin($turnin)
            ->withUser($user);
    }

    public function show(Assignment $assignment, Group $group)
    {
        $users = User::all();
        return view('test', compact('assignment', 'users', 'group'));
    }

    public function gradeView(Turnin $turnin, Group $group, Assignment $assignment)
    {
        return view('grade', compact('turnin', 'assignment', 'group'));
    }

    public function addGrade(Request $request, Turnin $turnin)
    {
        $this->validate($request, [
           'graded' => 'numeric|',
            'total' => 'numeric'
        ]);

        $turnin->grade = $request['grade'];
        $turnin->total = $request['total'];
        $turnin->save();
        return back();
    }
}
