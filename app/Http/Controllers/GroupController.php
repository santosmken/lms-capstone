<?php

namespace App\Http\Controllers;

use App\Assignment;
use App\Folder;
use App\Group;
use App\Post;
use App\Turnin;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Session;


class GroupController extends Controller
{
    //
    public function create(Request $request)
    {
        $group_code = str_random(5);
        $user = Auth::user()->id;

        //VALIDATION START's HERE

        $group = new Group();
        $group->college_year = $request['college_year'];
        $group->section      = $request['section'];
        $group->subject      = $request['subject'];
        $group->user_id = $user;
        $group->group_name = $group->section . '_' .$group->subject;
        $group->group_code = $group_code;
        $group->save();

        $group->users()->attach($user);
        return redirect()->route('dashboard.professor')->with([
            'user' => Auth::user(),
        ]);
    }

    public function show(Group $group)
    {
        $user = Auth::user();
        $users = User::all();
        $turnin = Turnin::all();
//        $assignments = Assignment::all();
        $assignments = Assignment::orderBy('body','asc')->get();
//        orderBy('timestamp','desc')->get();
        return view('group.inside_group', compact('group', 'user', 'assignments', 'users'))->with([
            'turnin' => $turnin,
        ]);
    }

    public function index() // Manage Groups
    {
        $groups = Group::all();
        return view('group._manage_groups')->with([
            'groups'=> $groups,
            'user'  =>  Auth::user(),
        ]);
    }



    public function members($id)
    {
        $user = Auth::user();
        $group = Group::findOrFail($id);
        $users = User::all();
        return view('group.group_members', compact('user', 'users', 'group'));
    }

    public function addFolder(Request $request,Group $group_id)
    {
//        $group = Group::find($group_id);

        $this->validate($request, [
           'folder_title' => 'required|min:3'
        ]);

        $folder = new Folder();
        $folder->folder_title = $request['folder_title'];
        $foldername = $folder->folder_title;
        $permission = intval(0775);
        Storage::disk('private')->makeDirectory($foldername, $permission, true);
        $folder->user_id = Auth::user()->id;

        $group_id->folders()->save($folder);

//        $group_id->folders()->save($folder);
//        $group->folders()->save($folder);
//        $id->folders()->save($folder);

        return back();
    }

    public function addMembers(User $user_id, Group $group)
    {
        $user_id->groups()->attach($user_id);
        return back()
            ->withGroup($group);

    }
}
