<?php

namespace App\Http\Controllers;


use App\Assignment;
use App\File;
use App\Group;
use App\Turnin;
use App\User;
use DB;
use Illuminate\Http\Request;
use App\Post;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $user = Auth::user();
        $posts = Post::latest()->get();
        $assignments = Assignment::latest()->get();
        $users = User::all();
        $groups = Group::all();
        $post = Post::with('groups')->get();
        $files = File::all();
        $turnin = Turnin::all();

//        $data = [];
//        $data['assignments'] = $assignments;
//        $data['posts'] = $posts;


        if(Auth::user()->role == 'student'){
            return view('student.dashboard_student')->with([
            'groups' => $groups,
            'posts' => $posts,
            'assignments' => $assignments,
            'users' => $users,
            'user' => Auth::user(),
            ]);
        }
        elseif (Auth::user()->role == 'professor'){
             return view('professor.dashboard_professor', compact(
                 'groups',
                 'posts',
                'turnin',
                 'assignments',
                 'post',
                 'user'
             ));
        }
        else{
            return view('admin.dashboard_admin')
                ->withFiles($files)
                ->withUsers($users)
                ->withUser(Auth::user());
        }
    }

    public function showPost($post_id)
    {
        $post = Post::all();
        $posts = Post::findOrFail($post_id);

        return view('view_post', compact($posts, $post))->withUser(Auth::user())->withPost($posts);
    }

    public function unpublishedPost()
    {
        $user = Auth::user();
        return view('view_unpublished')->withUser($user);
    }

}
