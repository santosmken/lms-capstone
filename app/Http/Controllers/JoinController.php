<?php

namespace App\Http\Controllers;

use App\Group;
use DB;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Session;

class JoinController extends Controller
{
    public function join()
    {
        $user = Auth::user()->id;
        $group_code = Input::get('group_code');
        //CHECK if the group code entered isEXIST
        $group = Group::where('group_code', '=', $group_code)->first();
        //Check if the user EXIST already in the group
        $exist = DB::table('group_user')->where('user_id', '=', $user)->get();

        if ($exist){
            Session::flash('warning', 'Already part of this group');
            return back();
        }
        elseif ($group){
            $group->users()->attach($user);
            Session::flash('success', 'You are now part of the group!');
            return redirect()->back();
        }
//        $group_code != $group
        elseif (!$group){
            Session::flash('danger', 'Wrong Group Code!');
            return back();
        }
    }
}
