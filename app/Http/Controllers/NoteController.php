<?php

namespace App\Http\Controllers;

use App\Assignment;
use App\Notes;
use App\Turnin;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class NoteController extends Controller
{
    public function create(Request $request, Turnin $turnin)
    {

        $this->validate($request, [
           'note' => 'min:5|max:60'
        ]);

        $note = new Notes();
        $note->note = $request['note'];
        $note->grade = $request['grade'];
        $note->total = $request['total'];
        $note->user_id = Auth::user()->id;
        $turnin->notes()->save($note);
        return back();
//        return view('group.assignment_show')
//            ->withAssignment($assignment);

    }

}
