<?php

namespace App\Http\Controllers;

use App\Assignment;
use App\Group;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class PostController extends Controller
{
    /*-----------------POST----------------*/

    public function post(Request $request, Group $group)
    {
        $groups = Group::all();
        $user = Auth::user()->id;

        $this->validate($request, [
            'body'   =>  'required|max:120',
        ]);

        $post = new Post();
        $post->body = $request['body'];
        $post->user_id = $user;
        if ($request->hasFile('file'))
        {
            $file = $request->file('file');
            $filename = time() . '.' .$file->getClientOriginalExtension();
            $originalname = $file->getClientOriginalName();
            $location = public_path('attachments/');
            //public_path = base location need ung / . $filename
            //pag may . $filename magiging nested ung storing ng files
            $file->move($location, $file->getClientOriginalName());
            $post->file_name = $originalname;
            $post->old_file_name = $filename;
        }
        $post->save();
        $post->groups()->sync($request->groups, false);

        Session::flash('success', 'Post successfully created!');

        return redirect()->back()
            ->withGroups($groups)
            ->withUser(Auth::user());
    }

    public function create(Request $request, Group $group)
    {
        $this->validate($request, [
            'body'   =>  'required|max:120',
        ]);

        $post = new Post();
        $post->body = $request['body'];
        $post->user_id = Auth::user()->id;

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $filename = time() . '.' .$file->getClientOriginalExtension();
            $originalname = $file->getClientOriginalName();
            $location = public_path('attachments/');
            $file->move($location, $file->getClientOriginalName());
            $post->file_name = $originalname;
            $post->old_file_name = $filename;
        }

        $group->posts()->save($post);
        Session::flash('success', 'Message successfully posted!');
        return back()->withUser(Auth::user());

    }

    public function edit($post_id)
    {
        $post = Post::find($post_id);
        return back(compact('post'));
    }

    public function updatePost(Request $request, $post_id)
    {
        $post = Post::find($post_id);
        $post->body = $request->body;
        $post->save();

        Session::flash('success', 'Message Successfully updated!');
        return back();
    }

    public function getDeletePost(Post $post)
    {
        $path = public_path('attachments'). '/' . $post->file_name;
        if (Auth::user() != $post->user) {
            return redirect()->back();
        }
        if (empty($post->file_name)){
            $post->delete();
            Session::flash('success', 'Successfully Deleted!');
            return back();
        }else{
            $post->delete();
            unlink($path);
            Session::flash('success', 'Successfully Deleted!');
            return back();
        }
    }



}
