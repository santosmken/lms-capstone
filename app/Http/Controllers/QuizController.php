<?php

namespace App\Http\Controllers;

use App\Group;
use App\Question;
use App\Quiz;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class QuizController extends Controller
{
    public function show(Request $request, Group $group_id)
    {
        return view('quiz.multiple_choice')
            ->withGroup($group_id)
            ->withUser(Auth::user());
    }

    public function create(Request $request, $group_id, Quiz $quiz)
    {
        $quiz = new Quiz();
        $quiz->title = $request['title'];
        $quiz->type = $request['type'];
        $quiz->time_limit = $request['time_limit'];
        $quiz->user_id = Auth::user();
//        $group_id->quizzes()->save($quiz);

        if ($request['type'] == "tf")
        {
            return 'tf';
        }
        elseif($request['type'] == 'mc'){
//            return view('quiz.multiple_choice', compact('group_id', 'quiz'));
            return redirect('quiz/group/{group_id}');
        }
        else{
            return 'none of the above';
        }
    }


    public function addQuestion(Request $request, Quiz $quiz_id, Group $group)
    {
        $this->validate($request, [
            'title' => 'required',
            'time_limit' => 'required|numeric',
           'question' => 'required|max:200',
           'first_answer' => 'required|max:160' ,
           'second_answer' => 'required|max:160',
           'third_answer' => 'required|max:160',
           'fourth_answer' => 'required|max:160'
        ]);

        $quiz = new Quiz();
        $quiz->title = $request['title'];
        $quiz->time_limit = $request['time_limit'];
        $group->quizzes()->save($quiz_id);

        $question = new Question();
        $question->question = $request['question'];
        $question->first_answer = $request['first_answer'];
        $question->second_answer = $request['second_answer'];
        $question->third_answer = $request['third_answer'];
        $question->fourth_answer = $request['fourth_answer'];
        $quiz_id->questions()->save($question);

        return redirect()->back();
    }

    public function getMC(Group $group)
    {
        $group = Group::find($group);
        return view('quiz.multiple_choice')
            ->withGroup($group)
            ->withUser(Auth::user());

    }
}
