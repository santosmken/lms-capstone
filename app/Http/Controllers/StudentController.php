<?php

namespace App\Http\Controllers;

use App\Assignment;
use App\Comment;
use App\Group;
use App\Post;
use App\Turnin;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class StudentController extends Controller
{
    //

    public function group($id) //Find Specific Groups
    {
        $group = Group::findOrFail($id);
        $comments = Comment::all();
        $users = User::all();
        $posts = Post::all()->sortBy('created_at');
        $user = Auth::user();
        $turnin = Turnin::all();

        return view('student.student_group', compact('turnin'))
            ->withGroup($group)
            ->withComments($comments)
//            ->withTurnIn($turnin)
            ->withPosts($posts)
            ->withUsers($users)
            ->withUser($user);
    }

    public function groupPost(Request $request, Group $group)
    {
        $this->validate($request, [
            'body' => 'required|max:120',
        ]);

        $user = Auth::user()->id;

        $post = new Post();
        $post->body = $request['body'];
        $post->user_id = $user;
        if ($request->hasFile('file'))
        {
            $file = $request->file('file');
            $filename = time() . '.' .$file->getClientOriginalExtension();
            $originalname = $file->getClientOriginalName();
            $location = public_path('attachments/');
            $file->move($location, $file->getClientOriginalName());
            $post->file_name = $originalname;
            $post->old_file_name = $filename;
        }

        $group->posts()->save($post);
        return redirect()->back()
            ->withGroup($group);
    }

    public function showGroups() //Manage Groups - SideBar
    {
        $user = Auth::user();
        $groups = Group::all();

        return view('student.student_groups')
            ->withUser($user)
            ->withGroups($groups);
    }

    public function turnInAssignment($id) //Submit Assignment
    {
        $assignment = Assignment::findOrFail($id);
        $user = Auth::user();
        $turnin = Turnin::with('user');
        return view('student.student_assignment_submit')
            ->withTurnin($turnin)
            ->withAssignment($assignment)
            ->withUser($user);
    }

    public function locker()
    {
        $user = Auth::user();
        return view('student.student_personal_locker')
            ->withUser($user);
    }

    public function portfolio()
    {
        $user = Auth::user();
        $groups = Group::with('user');
        return view('student.student_profile')
            ->withGroups($groups)
            ->withUser($user);
    }


}
