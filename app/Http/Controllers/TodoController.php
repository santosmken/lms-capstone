<?php

namespace App\Http\Controllers;

use App\Todo;
use Illuminate\Http\Request;

use App\Http\Requests;

class TodoController extends Controller
{

    public $restful = true;

    public function postAdd(Request $request)
    {
        $todo = new Todo();
        $todo->title = $request->title;
        $todo->save();
        $last_todo = $todo->id;
        $todos = Todo::whereId($last_todo)->get();
        return view('todo.ajaxData')
            ->withTodos($todos);
    }

    public function postUpdate(Request $request,$id)
    {
        $task = Todo::find($id);
        $task->title = $request->title;
        $task->save();

        return 'Okay';
    }

    public function getIndex()
    {
        $todos = Todo::all();
        return view('todo.index')
            ->withTodos($todos);
    }
}
