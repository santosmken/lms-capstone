<?php

namespace App\Http\Controllers;

use App\Assignment;
use App\Turnin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;

class TurnInController extends Controller
{
//    public function store(Request $request)
//    {
//        $input = Request::all();
//        $input['user_id'] = Auth::user()->id;
//        $input['previous_filename'] = Request::file('file')->getClientOriginalName();
//        $input['filename'] = uniqid() . "_" . rand(10000, 99999) . "." .
//            Request::file('file')->getClientOriginalExtension();
//        Request::file('file')->move('attachment/', $input['filename']);
//        Turnin::create($input);
//
//        return Redirect::route('student.turnedin', $input['assignment_id']);
//    }

    public function create(Request $request, Assignment $assignment)
    {
        $this->validate($request, [
            'description' => 'required|max:120',
           'file' => 'required',
        ]);

        $turnin = new Turnin();
        $turnin->description = $request['description'];
        $turnin->user_id = Auth::user()->id;

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $filename = time() . '.' .$file->getClientOriginalExtension();
            $originalname = $file->getClientOriginalName();
            $location = public_path('attachments/');
            $file->move($location, $file->getClientOriginalName());
            $turnin->previous_filename = $originalname;
            $turnin->filename = $filename;
        }
        $assignment->turnins()->save($turnin);
        return back();

    }
}
