<?php

namespace App\Http\Controllers;

use App\Group;
use App\Tag;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{

    public function getProfile()
    {
        $user = Auth::user();
        return view('pages.profile')
            ->withUser($user);
    }

    public function update_avatar(Request $request)
    {
        $user = Auth::user();
        $user->secondary_email = $request->secondary_email;
        if ($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(500,500)->save( public_path('/uploads/avatars/' . $filename) );

            $user = $user;
            $user->avatar = $filename;

        }
        $user->save();
        return redirect()->route('pages.profile');
    }

    public function getChangePassword()
    {
        $user = Auth::user();
        return view('pages.change_password')
            ->withUser($user);
    }

    public function changePassword(Request $request, User $user)
    {
        $this->validate($request, [
           'current_password' => 'required',
            'new_password' => 'required|min:6|max:60|confirmed'
        ]);


//        $user->password = $request['current_password'];

        if ($request['current_password'] === $user->password){
            return 'Okay';
        }
        else{
            return 'wrong current password';
        }
//        $user->password = $request['new_password'];


    }

}
