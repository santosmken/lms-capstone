<?php


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use App\Post;

Route::get('ajax', function (){
    $posts = Post::latest()->get();
   return view('ajax', compact('posts'));
});
Route::get('test/data', function (){
   $post = DB::table('posts')
       ->select("body","created_at", "file_name", "user_id");
    $assignment = DB::table("assignments")
        ->select("title", "body", "due_time", "due_date")
        ->unionAll($post)
        ->get();

});


//-->Log-in
Route::get('/', ['uses' => 'HomeController@index', 'as' => 'dashboard.professor', 'middleware' => 'auth']);
Route::auth();

//-->Forgot Password
Route::get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
Route::post('password/reset', 'Auth\PasswordController@reset');

//-->Authenticated User [ROUTE group]
Route::group(['middleware' => ['auth']], function (){

    //-->Update Profile
    Route::get('profile', ['uses' => 'UserController@getProfile', 'as' => 'pages.profile',]);
    Route::post('profile', 'UserController@update_avatar');
    //-->Change Password
    Route::get('change/password/{user}', 'UserController@getChangePassword');
    Route::post('change/password/{user}', 'UserController@changePassword');

    //-->Manage Own Group(s)
    Route::get('manage/groups', ['uses' => 'GroupController@index', 'as' => 'manage.group',]);
    //-->Join a group
    Route::post('join', 'JoinController@join');
    //-->Members of the group
    Route::get('group/members/{id}', 'GroupController@members');

    //-->Files and Folder [LOCKER]
    Route::get('personal/locker', 'FileController@index');

    //-->Add and Delete File
    Route::post('add/file', 'FileController@store');
    Route::get('delete/file/{file}', 'FileController@deleteFile');

    //-->Add and Delete Folder
    Route::post('add/folder', 'FileController@createFolder');
    Route::get('delete/folder/{folder}', 'FileController@deleteFolder');
    Route::get('folder/{folder_id}', 'FileController@getFolder');

    //Upload file inside the folder
    Route::post('folder/{folder}/files', 'FileController@addFilesToFolder');

    //Group Folder
    Route::get('group/folder/{folder_id}', 'FileController@groupFolder');
    Route::post('folder/{group_id}', 'GroupController@addFolder');


    //Download File in the backpack
    Route::get('get/download/{file_id}', 'FileController@downloadFile');
    //Download File in the group
    Route::get('download/{post_id}', 'FileController@downloadGroupFile');
    //Download Turned In
    Route::get('download/turnedin/{turnin_id}', 'FileController@downloadTurnedInFile');

    //-->Edit Post and Assignment
    Route::get('update/post/{post_id}', 'PostController@getPost');
    Route::get('update/assignment/{assignment_id}', 'AssignmentController@getAssignment');

    //-->Update Post and Assignment
    Route::post('update/post/{post_id}', 'PostController@updatePost');
    Route::post('update/assignment/{assignment_id}', 'AssignmentController@updateAssignment');

    //-->Delete Post and Assignment
    Route::get('delete/post/{post}', 'PostController@getDeletePost');
    Route::get('delete/assignment/{assignment_id}', 'AssignmentController@getDeleteAssignment');

    //-->Comment
    Route::post('/comment/{post_id}', 'CommentController@create');
    Route::post('/comment/assignment/{assignment_id}', ['uses' => 'CommentController@commentAssignment', 'as' => 'comment.assignment']);


    /* Group -> POST */
    Route::post('groups/{group}/posts', [
        'uses' => 'PostController@create',
        'as' => 'post.create',
    ]);

    Route::post('home/post', 'PostController@post');
    Route::get('home/post/{post_id}', 'PostController@edit');
    Route::post('home/post/{post_id', 'PostController@updatePost');
});

Route::group(['middleware' => ['auth', 'admin']], function () {

    Route::post('add/user', 'AdminController@addUser');
    Route::get('all/users', 'AdminController@allUsers');
    Route::get('user/{user}', 'AdminController@edit');
    Route::get('delete/user/{user}', 'AdminController@deleteUser');
});

Route::group(['middleware' => ['auth','student']], function() {

    Route::get('home/group/{id}', [
        'uses' => 'StudentController@group',
        'as' => 'student.group',
    ]);

    Route::post('/student/create/post/{group}',[
       'uses' => 'StudentController@groupPost',
        'as'  => 'student.post'
    ]);

    Route::get('/student/groups/', [
       'uses' => 'StudentController@showGroups',
        'as'  => 'student.groups'
    ]);

    Route::get('/group/members',[
       'uses' => 'StudentController@showMembers',
        'as'  => 'group.members'
    ]);

    Route::get('/student/{id}/submission', [
        'uses'  => 'StudentController@turnInAssignment',
        'as'   => 'student.turnedin'
    ]);

    Route::post('/student/{assignment}/submission', [
        'uses'  => 'TurninController@create',
        'as'   => 'assignment.turnin'
    ]);

    Route::get('/student/locker',['uses' => 'StudentController@locker', 'as' => 'student.locker']);

    Route::get('eportfolio', 'StudentController@portfolio');
});


Route::group(['middleware' => ['auth','professor']], function() {


    Route::post('home/assignment', 'AssignmentController@assignment');

    /*----------GROUP CONTROLLER-----------*/
    Route::post('create/group', [
        'uses' => 'GroupController@create',     /*Create group*/
        'as' => 'group.create',
    ]);

    Route::get('group/{group}', [
        'uses' => 'GroupController@show',         /*-REDIRECTING TO GROUP SELECTED-*/
        'as' => 'group.get',
    ]);

    Route::get('/group/{group}/assignment/{id}', [
       'uses'  =>   'AssignmentController@show',
        'as'   =>   'turnin.assignment'
    ]);



    /* Group -> ASSIGNMENT */
    Route::post('groups/{group}/assignments', [
        'uses' => 'AssignmentController@create',
        'as' => 'post.assignment',

    ]);

    //---------------------------QUIZ------------------------------------//
    //1st Step GET PAGE
    /**/ Route::get('quiz/{group_id}', 'QuizController@show');/**/
    //2nd Step to POST SUBMIT with default values
    /**/ Route::post('quiz/{group_id}', 'QuizController@create');
    //---------------------------QUIZ------------------------------------//
    //TYPE
    Route::post('create/quiz/{group}', 'QuizController@addQuestion');

//    ----------------GRADING-----------------
//    Route::get('/group/{group}/assignment/{id}', ['uses' => 'AssignmentController@studentSubmitted', 'as' => 'student.submitted']);

    Route::get('/assignment/{assignment}/group/{group}', 'GradingController@show');

    Route::get('grading/{turnin}/{group}/{assignment}', ['uses' => 'GradingController@gradeView', 'as' => 'grading']);

    Route::post('graded/{turnin}', 'GradingController@addGrade');

    Route::post('add/note/{turnin}', ['uses' => 'NoteController@create', 'as' => 'graded']);

    Route::post('add/group/members/{user_id}', 'GroupController@addMembers');

});

