<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notes extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function turnin()
    {
        return $this->belongsTo(Turnin::class);
    }

}
