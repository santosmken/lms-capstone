<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
//    protected $fillable = [
//        'body',
//        'tags',
//        'published_at',
//    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class, 'group_post')->withTimestamps();
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }


//    public function setPublishedAtAttribute($date)
//    {
//        $this->attributes['published_at'] = Carbon::createFromFormat('Y-m-d', $date);
//    }
//
//    public function setPublishedTimeAttribute($time)
//    {
//        $this->attributes['published_time'] = Carbon::parse($time);
//    }
//
//
//    public function scopePublished($query)
//    {
//        $now = date('H:i:sa');
//
//        $query
//            ->where('published_at', '<=', Carbon::now())
//            ->where('published_time', '<', Carbon::now());
//    }
//
//    public function scopeUnpublished($query)
//    {
//        $query
//            ->where('published_at', '>', Carbon::now());
////            ->where('published_time', '<', Carbon::now());
//
//        //LATE POSTS
//    }


}
