<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Turnin extends Model
{
    //
    protected $fillable = [
        'description',
        'filename',
        'previous_filename',
        'assignment_id',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function assignment()
    {
        return $this->belongsTo(Assignment::class);
    }

    public function notes()
    {
        return $this->hasMany(Notes::class);
    }

    public function hasTurnInFromUser($userId)
    {
        return $this->turnins()->where('user_id', $userId)->count() > 0;
    }

    public function hasNotTurnInFromUser($userId)
    {
        return $this->turnins()->where('user_id', $userId)->count() < 0;

    }



}
