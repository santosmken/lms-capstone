<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTurninsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('turnins', function (Blueprint $table){

            $table->foreign('assignment_id')
                ->references('id')
                ->on('assignments')
                ->onDelete('cascade');
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->increments('id');
            $table->text('description');
            $table->string('filename');
            $table->string('previous_filename');
            $table->integer('grade')->unsigned()->nullable();
            $table->integer('total')->unsigned()->nullable();
            $table->boolean('active');

            $table->integer('assignment_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('turnins');
    }
}
