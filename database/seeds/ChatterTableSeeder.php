<?php

use Illuminate\Database\Seeder;

class ChatterTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        // CREATE THE USER

        if(!\DB::table('users')->find(1)){

            \DB::table('users')->insert(array (
                0 =>
                    array (
                        'id' => 1,
                        'name' => 'Tony Lea',
                        'email' => 'tony@hello.com',
                        'password' => '$2y$10$9ED4Exe2raEeaeOzk.EW6uMBKn3Ib5Q.7kABWaf4QHagOgYHU8ca.',
                        'remember_token' => 'RvlORzs8dyG8IYqssJGcuOY2F0vnjBy2PnHHTX2MoV7Hh6udjJd6hcTox3un',
                        'created_at' => '2016-07-29 15:13:02',
                        'updated_at' => '2016-08-18 14:33:50',
                    ),
            ));

        }

        // CREATE THE CATEGORIES

        \DB::table('chatter_categories')->delete();

        \DB::table('chatter_categories')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'parent_id' => NULL,
                    'order' => 1,
                    'name' => 'Introductions',
                    'color' => '#3498DB',
                    'slug' => 'introductions',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            1 =>
                array (
                    'id' => 2,
                    'parent_id' => NULL,
                    'order' => 2,
                    'name' => 'General',
                    'color' => '#2ECC71',
                    'slug' => 'general',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            2 =>
                array (
                    'id' => 3,
                    'parent_id' => NULL,
                    'order' => 3,
                    'name' => 'Feedback',
                    'color' => '#9B59B6',
                    'slug' => 'feedback',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            3 =>
                array (
                    'id' => 4,
                    'parent_id' => NULL,
                    'order' => 4,
                    'name' => 'Random',
                    'color' => '#E67E22',
                    'slug' => 'random',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
        ));



        // CREATE THE DISCUSSIONS

        \DB::table('chatter_discussion')->delete();

        \DB::table('chatter_discussion')->insert(array (
            0 =>
                array (
                    'id' => 3,
                    'chatter_category_id' => 1,
                    'title' => 'Hello Everyone, This is my Introduction',
                    'user_id' => 1,
                    'sticky' => 0,
                    'views' => 0,
                    'answered' => 0,
                    'created_at' => '2016-08-18 14:27:56',
                    'updated_at' => '2016-08-18 14:27:56',
                    'slug' => 'hello-everyone-this-is-my-introduction',
                    'color' => '#239900',
                ),
            1 =>
                array (
                    'id' => 6,
                    'chatter_category_id' => 2,
                    'title' => 'Login Information for Chatter',
                    'user_id' => 1,
                    'sticky' => 0,
                    'views' => 0,
                    'answered' => 0,
                    'created_at' => '2016-08-18 14:39:36',
                    'updated_at' => '2016-08-18 14:39:36',
                    'slug' => 'login-information-for-chatter',
                    'color' => '#1a1067',
                ),
            2 =>
                array (
                    'id' => 7,
                    'chatter_category_id' => 3,
                    'title' => 'Leaving Feedback',
                    'user_id' => 1,
                    'sticky' => 0,
                    'views' => 0,
                    'answered' => 0,
                    'created_at' => '2016-08-18 14:42:29',
                    'updated_at' => '2016-08-18 14:42:29',
                    'slug' => 'leaving-feedback',
                    'color' => '#8e1869',
                ),
            3 =>
                array (
                    'id' => 8,
                    'chatter_category_id' => 4,
                    'title' => 'Just a random post',
                    'user_id' => 1,
                    'sticky' => 0,
                    'views' => 0,
                    'answered' => 0,
                    'created_at' => '2016-08-18 14:46:38',
                    'updated_at' => '2016-08-18 14:46:38',
                    'slug' => 'just-a-random-post',
                    'color' => '',
                ),
            4 =>
                array (
                    'id' => 9,
                    'chatter_category_id' => 2,
                    'title' => 'Welcome to the National University Community Forum ',
                    'user_id' => 1,
                    'sticky' => 0,
                    'views' => 0,
                    'answered' => 0,
                    'created_at' => '2016-08-18 14:59:37',
                    'updated_at' => '2016-08-18 14:59:37',
                    'slug' => 'welcome-to-the-national-university-community-forum',
                    'color' => '',
                ),
        ));

        // CREATE THE POSTS

        \DB::table('chatter_post')->delete();

        \DB::table('chatter_post')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'chatter_discussion_id' => 3,
                    'user_id' => 1,
                    'body' => '<p>My name is Kenneth and I\'m a developer at <a href="https://www.facebook.com/NationalUniversityManila/?fref=ts" target="_blank">https://www.facebook.com/NationalUniversityManila/?fref=ts</a> and I also work with an awesome organization in NU called The Wizards Circle: <a href="https://www.facebook.com/NUWizardCircle/?fref=ts" target="_blank">https://www.facebook.com/NUWizardCircle/?fref=ts</a></p>
        <p>You can check me out on twitter at <a href="https://twitter.com/MKESantos" target="_blank">https://twitter.com/MKESantos</a></p>',
                    'created_at' => '2016-08-18 14:27:56',
                    'updated_at' => '2016-08-18 14:27:56',
                ),
            1 =>
                array (
                    'id' => 5,
                    'chatter_discussion_id' => 6,
                    'user_id' => 1,
                    'body' => '<p>Hey!</p>
        <p>Thanks again for checking out forum. If you want to login with the default user you can login with the following credentials:</p>
        <p><strong>email address</strong>: santos@national-u.edu.ph</p>
        <p><strong>password</strong>: password</p>
        <p>You\'ll probably want to delete this user, but if for some reason you want to keep it... Go ahead :)</p>',
                    'created_at' => '2016-08-18 14:39:36',
                    'updated_at' => '2016-08-18 14:39:36',
                ),
            2 =>
                array (
                    'id' => 6,
                    'chatter_discussion_id' => 7,
                    'user_id' => 1,
                    'body' => '<p>If you would like to leave some feedback or have any issues be sure to visit the github page here: <a href="https://github.com/santosmken" target="_blank">https://github.com/santosmken</a>&nbsp;and I\'m sure I can help out.</p>
        <p>Feel free to contribute and share your ideas :)</p>',
                    'created_at' => '2016-08-18 14:42:29',
                    'updated_at' => '2016-08-18 14:42:29',
                ),
            3 =>
                array (
                    'id' => 7,
                    'chatter_discussion_id' => 8,
                    'user_id' => 1,
                    'body' => '<p>This is just a random post to show you some of the formatting that you can do in the WYSIWYG editor. You can make your text <strong>bold</strong>, <em>italic</em>, or <span style="text-decoration: underline;">underlined</span>.</p>
        <p style="text-align: center;">Additionally, you can center align text.</p>
        <p style="text-align: right;">You can align the text to the right!</p>
        <p>Or by default it will be aligned to the left.</p>
        <ul>
        <li>We can also</li>
        <li>add a bulleted</li>
        <li>list</li>
        </ul>
        <ol>
        <li><span style="line-height: 1.6;">or we can</span></li>
        <li><span style="line-height: 1.6;">add a numbered list</span></li>
        </ol>
        <p style="padding-left: 30px;"><span style="line-height: 1.6;">We can choose to indent our text</span></p>
        <p><span style="line-height: 1.6;">Post links: <a href="https://www.facebook.com/profile.php?id=100000260313831" target="_blank">https://www.facebook.com/profile.php?id=100000260313831</a></span></p>
        <p><span style="line-height: 1.6;">and add images:</span></p>
        <p><span style="line-height: 1.6;"><img src="https://media.giphy.com/media/o0vwzuFwCGAFO/giphy.gif" alt="" width="300" height="300" /></span></p>',
                    'created_at' => '2016-08-18 14:46:38',
                    'updated_at' => '2016-08-18 14:46:38',
                ),
            4 =>
                array (
                    'id' => 8,
                    'chatter_discussion_id' => 8,
                    'user_id' => 1,
                    'body' => '<p>Haha :) Cats!</p>
        <p><img src="https://media.giphy.com/media/5Vy3WpDbXXMze/giphy.gif" alt="" width="250" height="141" /></p>
        <p><img src="https://media.giphy.com/media/XNdoIMwndQfqE/200.gif" alt="" width="200" height="200" /></p>',
                    'created_at' => '2016-08-18 14:55:42',
                    'updated_at' => '2016-08-18 15:45:13',
                ),
            5 =>
                array (
                    'id' => 9,
                    'chatter_discussion_id' => 9,
                    'user_id' => 1,
                    'body' => '<p>Hey There!</p>
        <p>My name is Kenneth and I\'m the creator of this forum that you\'ve using. Thanks for checking out it out and if you have any questions or want to contribute be sure to checkout the repo here: <a href="https://github.com/santosmken" target="_blank">https://github.com/santosmken</a></p>
        <p>Happy programming with Laravel!</p>',
                    'created_at' => '2016-08-18 14:59:37',
                    'updated_at' => '2016-08-18 14:59:37',
                ),
            6 =>
                array (
                    'id' => 10,
                    'chatter_discussion_id' => 9,
                    'user_id' => 1,
                    'body' => '<p>Hell yeah Chad Shy!</p>
        <p><img src="https://media.giphy.com/media/j5QcmXoFWl4Q0/giphy.gif" alt="" width="366" height="229" /></p>',
                    'created_at' => '2016-08-18 15:01:25',
                    'updated_at' => '2016-08-18 15:01:25',
                ),
        ));


    }
}
