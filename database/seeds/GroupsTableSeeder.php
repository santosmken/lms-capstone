<?php

use Illuminate\Database\Seeder;
use App\Group;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $group = new Group();
        $group->group_name = "INF132 Capstone-2";
        $group->college_year  = "4th Year";
        $group->section      = "INF132";
        $group->subject   = "Capstone-2";
        $group->group_code   = "abc123";
        $group->user_id   = "1";
        $group->save();
    }
}
