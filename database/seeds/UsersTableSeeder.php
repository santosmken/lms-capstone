<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $group = App\Group::find(1);

        $professor = new User();
        $professor->first_name = "Mark Kenneth E.";
        $professor->last_name  = "Santos";
        $professor->email      = "santosmke@national-u.edu.ph";
        $professor->role       = "professor";
        $professor->groups()->attach($group);
        $professor->password   = bcrypt('professor');
        $professor->save();

        $professor = new User();
        $professor->first_name = "John Mark A.";
        $professor->last_name  = "Dela Cruz";
        $professor->email      = "delacruzjma@national-u.edu.ph";
        $professor->role       = "admin";
        $professor->groups()->attach($group);
        $professor->password   = bcrypt('professor');
        $professor->save();

        $professor = new User();
        $professor->first_name = "Chad S";
        $professor->last_name  = "Shy";
        $professor->email      = "shychads@national-u.edu.ph";
        $professor->groups()->attach($group);
        $professor->password   = bcrypt('professor');
        $professor->save();

        $professor = new User();
        $professor->first_name = "Reuben Louis";
        $professor->last_name  = "Jusay";
        $professor->email      = "jusayrl@national-u.edu.ph";
        $professor->password   = bcrypt('professor');
        $professor->save();
    }
}
