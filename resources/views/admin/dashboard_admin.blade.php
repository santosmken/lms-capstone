@extends('layouts.master')

@section('title')
    Admin
@endsection

@section('stylesheet')

@endsection

@section('content')

    @include('admin.dashboard_sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <!-- Your Page Content Here -->
            <div class="content">
                <div class="row">

                    <div class="col-md-4">
                        <div class="info-box">
                            <!-- Apply any bg-* class to to the icon to color it -->
                            <span class="info-box-icon bg-red"><i class="fa fa-users"></i></span>
                            <div class="info-box-content">
                                <h5><strong>Users</strong></h5>
                                <span class="info-box-number">{{$users->count()}}</span>
                            </div><!-- /.info-box-content -->
                        </div><!-- /.info-box -->
                    </div>

                    <div class="col-md-4">
                        <div class="info-box">
                            <!-- Apply any bg-* class to to the icon to color it -->
                            <span class="info-box-icon bg-blue-gradient"><i class="fa fa-archive"></i></span>
                            <div class="info-box-content">
                                <h5><strong>Uploads</strong></h5>
                                <span class="info-box-number">{{$files->count()}}</span>
                            </div><!-- /.info-box-content -->
                        </div><!-- /.info-box -->
                    </div>

                    <div class="col-md-4">
                        <div class="info-box">
                            <!-- Apply any bg-* class to to the icon to color it -->
                            <span class="info-box-icon bg-green-active"><i class="fa fa-pencil-square-o"></i></span>
                            <div class="info-box-content">
                                <h5><strong>Posts</strong></h5>
                                <span class="info-box-number">93,139</span>
                            </div><!-- /.info-box-content -->
                        </div><!-- /.info-box -->
                    </div>

                </div>
            </div>
        </section>
    </div>

    @include('admin.partials.add_user')
    @include('admin.partials.add_section')
    @include('admin.partials.add_subject')

@endsection