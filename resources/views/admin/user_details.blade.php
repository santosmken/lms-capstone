@extends('layouts.navigation')

@section('stylesheets')

    <style>
        .well{
            background-color: white;
        }
    </style>
@endsection

@section('content')
    <br>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="well text-center">
                    <img src="/uploads/avatars/{{$user->avatar}}" alt="" class="img img-circle" style="width: 150px; height: 150px;">
                    <h5>{{$user->first_name .' '.$user->last_name}}</h5>
                    <span class="label label-primary">{{ucfirst($user->role)}}</span>
                    <hr>

                    <p class="pull-left"><i class="fa fa-envelope"></i> {{$user->email}}</p>
                    <br><br><br><br>
                </div>


            </div>

            <div class="col-md-8">
                <div class="well">
                    <h5><strong>EDIT USER INFORMATION</strong></h5><br>

                    <form class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2" for="name">Name</label>
                            <div class="col-sm-10">
                                <input type="name" class="form-control" id="name" value="{{$user->first_name .' '.$user->last_name}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2" for="email">Email</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" id="email" value="{{$user->email}}">
                            </div>
                        </div>

                        <hr>

                        <div class="form-group">
                            <label class=" col-sm-2" for="password">Password</label>
                            <div class="col-sm-10">
                                <input type="text" name="password" id="password" class="form-control" placeholder="Leave Blank Unless You Want to Change Password">
                            </div>
                        </div>
                    </form>
                </div>

                <div class="well">
                    <button type="submit" class="btn btn-primary">Save User Changes</button>
                </div>
            </div>

        </div>
    </div>


@endsection