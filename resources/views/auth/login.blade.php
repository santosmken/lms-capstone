<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Welcome</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
</head>
<body>
<div class="container-fluid">
    <div class="row">

        <div class="col-md-8">
            <div class="row">
                <img src="{{asset('img/national-university-cover.png')}}" alt="" class="img-responsive">
            </div>
        </div>

        <div class="col-md-4">
            <div class="row">
                <div class="page-header">
                    @include('includes.message-block')
                    <img src="{{asset('img/national-university-header.png')}}" alt="">
                </div>
                <div class="col-md-10">
                    <div class="row">
                        Sign in with your school account
                        <br><br>
                        <form action="{{ url('/login') }}" method="post" role="form" class="inline">
                            {{ csrf_field() }}
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-user"></span>
                                </div>
                                <input id="email" type="email" class="form-control {{ $errors->has('email') ? ' has-error' : '' }}" name="email" value="{{ old('email') }}" placeholder="someone@national-u.edu.ph">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>

                            <br>

                            <div class="input-group">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-lock"></span>
                                </div>
                                <input id="password" type="password" class="form-control {{ $errors->has('password') ? ' has-error' : '' }}" name="password" placeholder="password">
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember"> Remember Me
                                </label>
                            </div>

                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-btn fa-sign-in"></i> Login
                            </button>

                            <a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@include('partials._javascript')
</body>
</html>