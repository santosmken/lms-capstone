<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>File Manager</title>
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="http://localhost:8000/vendor/laravel-filemanager/css/cropper.min.css">
  <link rel="stylesheet" href="http://localhost:8000/vendor/laravel-filemanager/css/lfm.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css">
</head>
<body>
  <div class="container">
    <div class="row fill">
      <div class="panel panel-primary fill">
        <div class="panel-heading">
          <h3 class="panel-title">Laravel FileManager</h3>
        </div>
        <div class="panel-body fill">
          <div class="row fill">
            <div class="wrapper fill">
              <div class="col-md-2 col-lg-2 col-sm-2 col-xs-2 left-nav fill" id="lfm-leftcol">
                <div id="tree1">
                </div>
              </div>
              <div class="col-md-10 col-lg-10 col-sm-10 col-xs-10 right-nav" id="right-nav">
                <div class="row">
                  <div class="col-md-12">
                                        <nav class="navbar navbar-default">
                      <div class="container-fluid">
                        <div class="navbar-header">
                          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                          </button>
                        </div>
                        <div class="collapse navbar-collapse">
                          <ul class="nav navbar-nav" id="nav-buttons">
                            <li>
                              <a href="#" id="to-previous">
                                <i class="fa fa-arrow-left"></i> Back
                              </a>
                            </li>
                            <li><a style='cursor:default;'>|</a></li>
                            <li>
                              <a href="#" id="add-folder">
                                <i class="fa fa-plus"></i> New Folder
                              </a>
                            </li>
                            <li>
                              <a href="#" id="upload" data-toggle="modal" data-target="#uploadModal">
                                <i class="fa fa-upload"></i> Upload
                              </a>
                            </li>
                            <li><a style='cursor:default;'>|</a></li>
                            <li>
                              <a href="#" id="thumbnail-display">
                                <i class="fa fa-picture-o"></i> Thumbnails
                              </a>
                            </li>
                            <li>
                              <a href="#" id="list-display">
                                <i class="fa fa-list"></i> List
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </nav>
                  </div>
                </div>

                
                <div id="content" class="fill">

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aia-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Upload File</h4>
        </div>
        <div class="modal-body">
          <form action="http://localhost:8000/laravel-filemanager/upload" role='form' id='uploadForm' name='uploadForm' method='post' enctype='multipart/form-data'>
            <div class="form-group" id="attachment">
              <label for='upload' class='control-label'>Choose File</label>
              <div class="controls">
                <div class="input-group" style="width: 100%">
                  <input type="file" id="upload" name="upload">
                </div>
              </div>
            </div>
            <input type='hidden' name='working_dir' id='working_dir' value='/shares'>
            <input type='hidden' name='show_list' id='show_list' value='0'>
            <input type='hidden' name='type' id='type' value='Images'>
            <input type='hidden' name='_token' value='5wAuZ5DN75gVBXwu9mWoLJqZH50Fqhc4Mfa7jegq'>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" id="upload-btn">Upload File</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="fileViewModal" tabindex="-1" role="dialog" aria-labelledby="fileLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="fileLabel">View File</h4>
        </div>
        <div class="modal-body" id="fileview_body">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.4.0/jquery-migrate.min.js"></script>
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.3.0/bootbox.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
  <script src="http://localhost:8000/vendor/laravel-filemanager/js/cropper.min.js"></script>
  <script src="http://localhost:8000/vendor/laravel-filemanager/js/jquery.form.min.js"></script>
  <script>
var ds            = '/';
var home_dir      = ds + "";
var shared_folder = ds + "shares";
var image_url     = "http://localhost:8000/photos";
var file_url      = "http://localhost:8000/files";

$(document).ready(function () {
  bootbox.setDefaults({locale:"en"});
  // load folders
  loadFolders();
  loadItems();
  setOpenFolders();
});

// ======================
// ==  Navbar actions  ==
// ======================

$('#nav-buttons a').click(function (e) {
  e.preventDefault();
});

$('#to-previous').click(function () {
  var working_dir = $('#working_dir').val();
  var last_ds = working_dir.lastIndexOf(ds);
  var previous_dir = working_dir.substring(0, last_ds);
  $('#working_dir').val(previous_dir);
  loadItems();
  setOpenFolders();
});

$('#add-folder').click(function () {
  bootbox.prompt("Folder name:", function (result) {
    if (result !== null) {
      createFolder(result);
    }
  });
});

$('#upload-btn').click(function () {
  var options = {
    beforeSubmit:  showRequest,
    success:       showResponse,
    error:         showError
  };

  function showRequest(formData, jqForm, options) {
    $('#upload-btn').html('<i class="fa fa-refresh fa-spin"></i> Uploading...');
    return true;
  }

  function showResponse(responseText, statusText, xhr, $form)  {
    $('#uploadModal').modal('hide');
    $('#upload-btn').html('Upload File');
    if (responseText != 'OK'){
      notify(responseText);
    }
    $('#upload').val('');
    loadItems();
  }

  function showError(jqXHR, textStatus, errorThrown) {
    $('#upload-btn').html('Upload File');
    if (jqXHR.status == 413) {
      notify('Request entity too large!');
    } else if (textStatus == 'error') {
      notify('An error has occured: ' + errorThrown);
    } else {
      notify('An error has occured: ' + textStatus + '<br>' + errorThrown);
    }
  }

  $('#uploadForm').ajaxSubmit(options);
  return false;
});

$('#thumbnail-display').click(function () {
  $('#show_list').val(0);
  loadItems();
});

$('#list-display').click(function () {
  $('#show_list').val(1);
  loadItems();
});

// ======================
// ==  Folder actions  ==
// ======================

$(document).on('click', '.folder-item', function (e) {
  clickFolder($(this).data('id'));
});

function clickFolder(new_dir) {
  $('#working_dir').val(new_dir);
  setOpenFolders();
  loadItems();
}

function dir_starts_with(str) {
  return $('#working_dir').val().indexOf(str) === 0;
}

function setOpenFolders() {
  var folders = $('.folder-item');

  for (var i = folders.length - 1; i >= 0; i--) {
    // close folders that are not parent
    if (! dir_starts_with($(folders[i]).data('id'))) {
      $(folders[i]).children('i').removeClass('fa-folder-open').addClass('fa-folder');
    } else {
      $(folders[i]).children('i').removeClass('fa-folder').addClass('fa-folder-open');
    }
  }
}

// ====================
// ==  Ajax actions  ==
// ====================

function loadFolders() {
  $.ajax({
    type: 'GET',
    dataType: 'html',
    url: 'http://localhost:8000/laravel-filemanager/folders',
    data: {
      working_dir: $('#working_dir').val(),
      show_list: $('#show_list').val(),
      type: $('#type').val()
    },
    cache: false
  }).done(function (data) {
    $('#tree1').html(data);
  });
}

function loadItems() {
  var working_dir = $('#working_dir').val();
  console.log('Current working_dir : ' + working_dir);

  $.ajax({
    type: 'GET',
    dataType: 'html',
    url: 'http://localhost:8000/laravel-filemanager/jsonitems',
    data: {
      working_dir: working_dir,
      show_list: $('#show_list').val(),
      type: $('#type').val()
    },
    cache: false
  }).done(function (data) {
    $('#content').html(data);
    $('#nav-buttons').removeClass('hidden');
    $('.dropdown-toggle').dropdown();
    setOpenFolders();
  });
}

function createFolder(folder_name) {
  $.ajax({
    type: 'GET',
    dataType: 'text',
    url: 'http://localhost:8000/laravel-filemanager/newfolder',
    data: {
      name: folder_name,
      working_dir: $('#working_dir').val(),
      type: $('#type').val()
    },
    cache: false
  }).done(function (data) {
    if (data == 'OK') {
      loadFolders();
      loadItems();
      setOpenFolders();
    } else {
      notify(data);
    }
  });
}

function rename(item_name) {
  bootbox.prompt({
    title: "Rename to:",
    value: item_name,
    callback: function (result) {
      if (result !== null) {
        $.ajax({
          type: 'GET',
          dataType: 'text',
          url: 'http://localhost:8000/laravel-filemanager/rename',
          data: {
            file: item_name,
            working_dir: $('#working_dir').val(),
            new_name: result,
            type: $('#type').val()
          },
          cache: false
        }).done(function (data) {
          if (data == 'OK') {
            loadItems();
            loadFolders();
          } else {
            notify(data);
          }
        });
      }
    }
  });
}

function trash(item_name) {
  bootbox.confirm("Are you sure you want to delete this item?", function (result) {
    if (result == true) {
      $.ajax({
        type: 'GET',
        dataType: 'text',
        url: 'http://localhost:8000/laravel-filemanager/delete',
        data: {
          working_dir: $('#working_dir').val(),
          items: item_name,
          type: $('#type').val()
        },
        cache: false
      }).done(function (data) {
        if (data != 'OK') {
          notify(data);
        } else {
          if ($('#working_dir').val() === home_dir || $('#working_dir').val() === shared_folder) {
            loadFolders();
          }
          loadItems();
        }
      });
    }
  });
}

function cropImage(image_name) {
  $.ajax({
    type: 'GET',
    dataType: 'text',
    url: 'http://localhost:8000/laravel-filemanager/crop',
    data: {
      img: image_name,
      working_dir: $('#working_dir').val(),
      type: $('#type').val()
    },
    cache: false
  }).done(function (data) {
    $('#nav-buttons').addClass('hidden');
    $('#content').html(data);
  });
}

function resizeImage(image_name) {
  $.ajax({
    type: 'GET',
    dataType: 'text',
    url: 'http://localhost:8000/laravel-filemanager/resize',
    data: {
      img: image_name,
      working_dir: $('#working_dir').val(),
      type: $('#type').val()
    },
    cache: false
  }).done(function (data) {
    $('#nav-buttons').addClass('hidden');
    $('#content').html(data);
  });
}

function download(file_name) {
  location.href = 'http://localhost:8000/laravel-filemanager/download?'
  + 'working_dir='
  + $('#working_dir').val()
  + '&type='
  + $('#type').val()
  + '&file='
  + file_name;
}

// ==================================
// ==  Ckeditor, Bootbox, preview  ==
// ==================================

function useFile(file) {

  function getUrlParam(paramName) {
    var reParam = new RegExp('(?:[\?&]|&)' + paramName + '=([^&]+)', 'i');
    var match = window.location.search.match(reParam);
    return ( match && match.length > 1 ) ? match[1] : null;
  }

  function useTinymce3(url) {
    var win = tinyMCEPopup.getWindowArg("window");
    win.document.getElementById(tinyMCEPopup.getWindowArg("input")).value = url;
    if (typeof(win.ImageDialog) != "undefined") {
      // Update image dimensions
      if (win.ImageDialog.getImageData) {
        win.ImageDialog.getImageData();
      }

      // Preview if necessary
      if (win.ImageDialog.showPreviewImage) {
        win.ImageDialog.showPreviewImage(url);
      }
    }
    tinyMCEPopup.close();
  }

  function useTinymce4AndColorbox(url, field_name) {
    parent.document.getElementById(field_name).value = url;

    if(typeof parent.tinyMCE !== "undefined") {
      parent.tinyMCE.activeEditor.windowManager.close();
    }
    if(typeof parent.$.fn.colorbox !== "undefined") {
      parent.$.fn.colorbox.close();
    }
  }

  function useCkeditor3(url) {
    if (window.opener) {
      // Popup
      window.opener.CKEDITOR.tools.callFunction(getUrlParam('CKEditorFuncNum'), url);
    } else {
      // Modal (in iframe)
      parent.CKEDITOR.tools.callFunction(getUrlParam('CKEditorFuncNum'), url);
      parent.CKEDITOR.tools.callFunction(getUrlParam('CKEditorCleanUpFuncNum'));
    }
  }

  function useFckeditor2(url) {
    var p = url;
    var w = data['Properties']['Width'];
    var h = data['Properties']['Height'];
    window.opener.SetUrl(p,w,h);
  }

  function getFileUrl(file) {
    var path = $('#working_dir').val();
    var item_url = image_url;

    
    if (path != ds) {
      item_url = item_url + path + ds;
    }

    var url = item_url + file;
    url = url.replace(/\\/g, "/");

    return url;
  }

  var url = getFileUrl(file);
  var field_name = getUrlParam('field_name');
  var is_ckeditor = getUrlParam('CKEditor');
  var is_fcke = typeof data != 'undefined' && data['Properties']['Width'] != '';

  if (window.opener || window.tinyMCEPopup || field_name || getUrlParam('CKEditorCleanUpFuncNum') || is_ckeditor) {
    if (window.tinyMCEPopup) { // use TinyMCE > 3.0 integration method
      useTinymce3(url);
    } else if (field_name) { // tinymce 4 and colorbox
      useTinymce4AndColorbox(url, field_name);
    } else if(is_ckeditor) { // use CKEditor 3.0 + integration method
      useCkeditor3(url);
    } else if (is_fcke) { // use FCKEditor 2.0 integration method
      useFckeditor2(url);
    } else { // standalone button or other situations
      window.opener.SetUrl(url);
    }

    if (window.opener) {
      window.close();
    }
  } else {
    alert('Editor not found, cannot apply image.');
  }
}
//end useFile

function notImp() {
  bootbox.alert('Not yet implemented!');;
}

function notify(x) {
  bootbox.alert(x);
}

function fileView(x) {
  var rnd = makeRandom();
  var img_src = image_url + $('#working_dir').val() + ds + x;
  var img = "<img class='img img-responsive center-block' src='" + img_src + "'>";
  $('#fileview_body').html(img);
  $('#fileViewModal').modal();
}

function makeRandom() {
  var text = '';
  var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

  for (var i = 0; i < 20; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
}

</script>
</body>
</html>
