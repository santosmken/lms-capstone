@extends('layouts.navigation')

@section('content')
    <br><br>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <ul class="list-group">
                    <li class="list-group-item"><i class="fa fa-pencil-square-o"></i>
                        <a href="/assignment/{{$assignment->id}}/group/{{$group->id}}"><strong> {{$assignment->title}}</strong></a>
                        <p class="text-muted">Due: {{date('M d, Y', strtotime($assignment->due_date))}}</p>
                    </li>
                    <li class="list-group-item">Showing: All</li>
                    <li class="list-group-item">{{$group->group_name}}</li>
                    {{--@foreach($assignment->turnins as $turnin)--}}
                        {{--<li class="list-group-item">--}}
                            {{--@foreach($group->users as $user)--}}
                                {{--<a href="{{url('grading', $turnin->id .'/'.$group->id . '/'. $assignment->id)}}">--}}
                                    {{--<strong>--}}
                                        {{--{{$user->id == $turnin->user_id ? "" .$turnin->user->first_name . " " .$turnin->user->last_name : ""}}--}}
                                    {{--</strong>--}}
                                {{--</a>--}}
                            {{--@endforeach--}}
                        {{--</li>--}}
                    {{--@endforeach--}}
                </ul>

            </div>

            <div class="col-md-9">
                <div class="well" style="background-color: white;">
                    <div class="row">
                        <div class="col-md-1">
                            <img src="/uploads/avatars/{{ $turnin->user->avatar }}" alt="" style="width: 50px; height: 40px;" class="img img-responsive">
                        </div>

                        <div class="col-md-5">
                            <h5><strong>{{$turnin->user->first_name .' '. $turnin->user->last_name}}</strong></h5>
                            <h6>Latest submission on {{date('M d, Y @ g:i A', strtotime($turnin->created_at))}}</h6>
                        </div>

                        <div class="col-md-5 col-md-offset-1">
                            <form action="{{url('graded', $turnin->id)}}" method="post">
                                {!! csrf_field() !!}
                                @if($turnin->grade >= 0)
                                    <div class="col-md-3">
                                        <input type="text" name="grade" class="form-control" value="{{$turnin->grade}}">
                                    </div>

                                    <div class="col-md-1">
                                        <p style="margin-top: 10px;">/</p>
                                    </div>

                                    <div class="col-md-3">
                                        <input type="text" name="total" class="form-control" value="{{$turnin->total}}">
                                    </div>

                                @else
                                    <div class="col-md-3">
                                        <input type="text" name="grade" class="form-control" value="{{$turnin->grade}}">
                                    </div>

                                    <div class="col-md-1">
                                        <p style="margin-top: 10px;">/</p>
                                    </div>

                                    <div class="col-md-3">
                                        <input type="text" name="total" class="form-control" value="{{$turnin->total}}">
                                    </div>
                                @endif

                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-primary">Grade</button>
                                </div>

                            </form>

                        </div>
                    </div>
                    <hr>

                    <p><strong>Message: </strong>{{$turnin->description}}</p>

                    <div class="well" style="background-color: white">
                        <a href="{{url('download/turnedin', $turnin->id)}}">{{$turnin->previous_filename}}</a>
                    </div>

                   <hr>
                    <textarea name="note" cols="30" rows="2" class="form-control" placeholder="Type your note here..."></textarea>
                    <br>
                    <button type="submit" class="btn btn-default col-md-offset-10" style="color: #00acd6">Add Comment</button>
                </div>


            </div>
        </div>
    </div>

    @include('sidebar.dashboard_control_sidebar')
@endsection


