@extends('layouts.master')

@section('title')
    Your Groups
@endsection

@section('stylesheets')
    <link rel="stylesheet" href="{{asset("css/style.css")}}">
@endsection

@section('content')
    @include('professor.dashboard_sidebar')

    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">

            <!-- Your Page Content Here -->
            <div class="content">
                <div class="row">
                    <div class="col-md-7">
                        <div class="panel panel-default panel-primary">
                            <div class="panel-heading">
                                <h3><strong>Groups</strong></h3>
                                <h6>Active</h6>
                            </div>

                            <div class="panel-body">
                                <ul class="list-group">
                                    @foreach($user->groups as $group_list )
                                        <li class="list-group-item">
                                            <a href="{{route('group.get', ['group_id' => $group_list->id])}}">{{$group_list->group_name}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    @include('sidebar.dashboard_control_sidebar')
    @include('group.partials._group_create')
    @include('group.partials._group_join')


@endsection
