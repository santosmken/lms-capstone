@extends('layouts.navigation')

@section('content')
    <br><br>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <ul class="list-group">
                    <li class="list-group-item"><i class="fa fa-file"></i><strong> {{$turnin->assignment->title}}</strong>
                        <p class="text-muted">Due {{date('M d Y', strtotime($turnin->assignment->due_date))}}</p>
                    </li>
                    <li class="list-group-item">Showing: All</li>
                    <li class="list-group-item"> Group name</li>
                    @foreach($assignment->turnin as $turnin)
                        {{$turnin->user->last_name}}
                    @endforeach
                    {{--@foreach($turnins as $turnin)--}}
                        {{--<li class="list-group-item">--}}
                            {{--@foreach($users as $user)--}}
                                {{--<a href="{{route('grading', $turnin->id)}}">--}}
                                    {{--<strong>{{ $user->id == $turnin->user_id ? ""--}}
                                     {{--. $user->first_name . " " . $user->last_name : ""}}</strong>--}}
                                {{--</a>--}}
                            {{--@endforeach--}}
                        {{--</li>--}}
                    {{--@endforeach--}}
                </ul>
            </div>

            <div class="col-md-9">
                <div class="panel panel-default" >
                    <form action="{{route('graded', $turnin->id)}}" method="post">
                        {!! csrf_field() !!}
                        <div class="panel-heading" style="background-color: white">
                            <div class="row">
                                <div class="col-md-5">
                                    <h5><strong>{{$turnin->user->first_name}}</strong></h5>
                                    <p>Latest Submission on {{date('M d, Y g:i A', strtotime($turnin->created_at))}}</p>
                                </div>

                                <div class="col-md-4 col-md-offset-3">
                                    <div class="row">


                                        @if($turnin->notes->count() > 0)
                                            <div class="col-md-3">
                                                <input type="text" name="grade" class="form-control" disabled>
                                            </div>

                                            <div class="col-md-1">
                                                <p style="margin-top: 10px;">/</p>
                                            </div>

                                            <div class="col-md-3">
                                                <input type="text" name="total" class="form-control" disabled>
                                            </div>

                                            <div class="col-md-2">
                                                <button type="submit" class="btn btn-primary" disabled>Graded</button>
                                            </div>
                                        @else
                                            <div class="col-md-3">
                                                <input type="text" name="grade" class="form-control">
                                            </div>

                                            <div class="col-md-1">
                                                <p style="margin-top: 10px;">/</p>
                                            </div>

                                            <div class="col-md-3">
                                                <input type="text" name="total" class="form-control" value="">
                                            </div>

                                            <div class="col-md-2">
                                                <button type="submit" class="btn btn-primary">Grade</button>
                                            </div>
                                        @endif
                                    </div>


                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <p>{{$turnin->description}}</p>
                            <div class="well" style="background-color: white">
                                <p>{{$turnin->previous_filename}}</p>
                            </div>
                        </div>

                        <div class="panel-footer">
                            <input type="text" name="note" class="form-control" placeholder="Type your note here..."><br>
                            <button type="submit" class="btn btn-warning col-md-offset-10">Add Comment</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

@endsection

