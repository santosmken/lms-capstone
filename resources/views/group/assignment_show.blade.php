@extends('layouts.navigation')

@section('content')
    <br><br>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <ul class="list-group">
                    <li class="list-group-item"><i class="fa fa-file"></i><strong> {{$assignment->title}}</strong>
                        <p class="text-muted">Due Sept, 25, 2016</p>
                    </li>
                    <li class="list-group-item">Showing: All</li>
                    <li class="list-group-item">{{$assignment->body}}</li>
                    @foreach($turnins as $turnin)
                        <li class="list-group-item">
                        @foreach($users as $user)
                                <a href="{{route('grading', $turnin->id)}}">
                                    <strong>{{ $user->id == $turnin->user_id ? ""
                                     . $user->first_name . " " . $user->last_name : ""}}</strong>
                                </a>
                        @endforeach
                        </li>
                    @endforeach

                </ul>
            </div>

            <div class="col-md-9">
                <div class="well" style="background-color: white;">
                    {{$assignment->title}}
                    <p class="text-muted"><strong>Due:</strong> {{date('F d Y @ g:i A')}}</p>

                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#home">Grading Overview</a></li>
                        <li><a href="#menu1">Assignment Detail</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="home" class="tab-pane fade in active">
                            <div class="well">
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-6">
                                        <div class="row">
                                            <div class="col-sm-3 col-sm-offset-2">
                                                <p><strong>1</strong></p>
                                            </div>

                                            <div class="col-sm-3" style="padding-left: 45px;">
                                                <p><strong>1</strong></p>
                                            </div>

                                            <div class="col-sm-3" style="padding-left: 45px;">
                                                <p><strong>0</strong></p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-4 col-sm-offset-1">
                                                <p class="text-primary">Not Turned In</p>
                                            </div>

                                            <div class="col-sm-3" style="padding-left: 30px;">
                                                <p class="text-primary">Ungraded</p>
                                            </div>

                                            <div class="col-sm-3" style="padding-left: 30px;">
                                                <p class="text-primary">Graded</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <p><strong>Name</strong></p>
                                </div>

                                <div class="col-md-3 col-md-offset-1">
                                    <p><strong>Latest Submission</strong></p>
                                </div>

                                <div class="col-md-2 col-md-offset-2">
                                    <p><strong>Grade</strong></p>
                                </div>
                            </div>

                                @foreach($turnins as $turnin)
                                    @foreach($users as $user)

                                    <div class="row">
                                        <div class="col-md-4">
                                            <p>
                                                {{ $user->id == $turnin->user_id ? ""
                                                 . $user->first_name . " " . $user->last_name : ""}}
                                            </p>
                                        </div>

                                        <div class="col-md-4 col-md-offset-1">
                                            {{ $user->id == $turnin->user_id ? ""
                                                . date('F d Y @ g:i A', strtotime($turnin->created_at)): ""}}
                                        </div>

                                    </div>
                                    @endforeach
                                @endforeach

                        </div>
                        <div id="menu1" class="tab-pane fade">
                            <br>
                            <p>Assigned to:</p>
                            <hr>
                            <p>{{$assignment->body}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('sidebar.dashboard_control_sidebar')
@endsection

@section('scripts')
    <script type="text/javascript">

        $(document).ready(function(){
            $(".nav-tabs a").click(function(){
                $(this).tab('show');
            });
        });

    </script>
@endsection

