@extends('layouts.master')

@section('content')


    @include('professor.dashboard_sidebar')
    <!-- Content Wrapper. Contains page content -->
    <section class="content-wrapper">

        <!-- Main content -->
        <section class="content">

            <!-- Your Page Content Here -->
            <div class="content">
                <div class="row">
                    <div class="col-md-7">
                        <div class="panel panel-primary">
                            <div class="panel-heading bg-light-blue">
                                <div class="panel-title">
                                    <h4><strong>{{$group->group_name}}</strong></h4>
                                    <h5>{{$group->user->first_name . ' ' .$group->user->last_name}}</h5>
                                </div>
                            </div>

                            <div class="panel-body">
                                <ul class="nav nav-tabs">
                                    <li><a href="#menu4">Posts</a></li>
                                    <li><a href="#menu5">Folders</a></li>
                                    <li><a href="{{url('/group/members', $group->id)}}">Members <span class="badge badge-success">{{$group->users->count()}}</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-7">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title">
                                    <h4><strong>Members</strong></h4>
                                </div>
                            </div>

                            <div class="panel-body">
                                @foreach($group->users as $user)
                                    <div class="row">
                                        <div class="col-md-2">
                                            <img src="{{asset('uploads/avatars', $user->avatar)}}" alt="">
                                        </div>

                                        <div class="col-md-6">
                                            <p>{{$user->first_name . ' ' . $user->last_name}}</p>
                                            <p>{{ucfirst($user->role)}}</p>
                                        </div>
                                    </div>
                                    <hr>

                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </section>
    </section>

    @include('sidebar.dashboard_control_sidebar')

@endsection