@extends('layouts.master')

@section('title')
    Welcome to the Group
@endsection

@section('stylesheets')

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset("css/select2.min.css")}}">
    <link rel="stylesheet" href="{{asset("css/bootstrap-timepicker.min.css")}}">

@endsection

@section('content')

    @include('professor.dashboard_sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">

            <!-- Your Page Content Here -->
            <div class="content">
                <div class="row">

                    <div class="col-md-7">
                        @include('includes.message-block')
                            <div class="panel panel-primary">
                                <div class="panel-heading bg-light-blue">
                                    <div class="panel-title">
                                        <h4><strong>{{$group->group_name}}</strong></h4>
                                        <h5>{{$group->user->first_name . ' ' .$group->user->last_name}}</h5>
                                    </div>
                                </div>

                        </div>
                        @include('group.partials.navigation_tabs')
                    </div>

                    <div class="col-md-3 col-md-offset-2">
                        <div class="panel panel-default ">
                            <div class="panel-heading bg bg-aqua-active">
                                <h5><strong>Access Code: <kbd>{{$group->group_code}}</kbd></strong></h5>
                            </div>

                            <div class="panel-body">
                                {{--<button class="btn btn-primary">Invite group members</button>--}}

                                <div class="row">
                                    <div class="col-md-10 col-md-offset-2">
                                        <button type="button" class="btn btn-default bg-yellow-active">Import Class List</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{--POST QUERY--}}
                    {{--<div class="row posts">--}}
                        {{--<div class="col-md-7">--}}
                            {{--<div class="panel panel-default">--}}
                                {{--<div class="panel-heading">--}}
                                    {{--<h4>Group Posts</h4>--}}
                                {{--</div>--}}
                            {{--</div>--}}


                            {{--@foreach($group->posts as $post)--}}
                                {{--@if(empty($post))--}}
                                    {{----}}
                                {{--@else--}}
                                    {{--<div class="panel panel-default">--}}
                                        {{--<div class="panel-body">--}}
                                            {{--<div class="row">--}}
                                                {{--<div class="col-md-2">--}}
                                                    {{--<img src="/uploads/avatars/{{ $post->user->avatar }}" alt="" class="img-bordered-sm img-responsive" style="height: 70px; width: 70px;">--}}
                                                {{--</div>--}}

                                                {{--<div class="col-md-10">--}}
                                                    {{--<div class="row">--}}
                                                        {{--<div class="col-md-10">--}}
                                                            {{--<p>{{$post->user->last_name}} to <a href="">{{$group->group_name}}</a></p>--}}

                                                        {{--</div>--}}

                                                        {{--@if(Auth::user() == $post->user)--}}
                                                            {{--<div class="col-md-2">--}}
                                                                {{--<div class="dropdown">--}}
                                                                    {{--<a type="button" data-toggle="dropdown"><i class="fa fa-caret-down"></i></a>--}}
                                                                    {{--<ul class="dropdown-menu">--}}
                                                                        {{--<li><a href="{{url('update/post', $post->id)}}" data-toggle="modal" data-target="#updatePost"><i class="fa fa-pencil"></i>Edit Post</a></li>--}}
                                                                        {{--<li><a href="{{url('delete/post', $post->id)}}"><i class="fa fa-trash"></i>Delete Post</a></li>--}}
                                                                        {{--<li><a href=""></a></li>--}}
                                                                    {{--</ul>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--@endif--}}

                                                    {{--</div>--}}
                                                    {{--<p><strong>{{$post->body}}</strong></p>--}}

                                                    {{--@if(!empty($post->file_name))--}}

                                                        {{--@if(pathinfo($post->file_name, PATHINFO_EXTENSION) == 'docx')--}}
                                                            {{--<div class="well" style="background-color: white;">--}}
                                                                {{--<div class="media">--}}
                                                                    {{--<div class="media-left">--}}
                                                                        {{--<i class="fa fa-file-word-o fa-3x" style="color: dodgerblue"></i>--}}
                                                                    {{--</div>--}}
                                                                    {{--<div class="media-body">--}}
                                                                        {{--<a href="{{url('download', $post->id)}}">{{$post->file_name}}</a>--}}
                                                                    {{--</div>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--@elseif(pathinfo($post->file_name, PATHINFO_EXTENSION) == 'pdf')--}}
                                                            {{--<div class="well" style="background-color: white;">--}}
                                                                {{--<div class="media">--}}
                                                                    {{--<div class="media-left">--}}
                                                                        {{--<i class="fa fa-file-pdf-o fa-3x" style="color: red"></i>--}}
                                                                    {{--</div>--}}
                                                                    {{--<div class="media-body">--}}
                                                                        {{--<a href="{{url('download', $post->id)}}">{{$post->file_name}}</a>--}}
                                                                    {{--</div>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--@elseif(pathinfo($post->file_name, PATHINFO_EXTENSION) == 'xlsx')--}}
                                                            {{--<div class="well" style="background-color: white;">--}}
                                                                {{--<div class="media">--}}
                                                                    {{--<div class="media-left">--}}
                                                                        {{--<i class="fa fa-file-excel-o fa-3x" style="color: green"></i>--}}
                                                                    {{--</div>--}}
                                                                    {{--<div class="media-body">--}}
                                                                        {{--<a href="{{url('download', $post->id)}}">{{$post->file_name}}</a>--}}
                                                                    {{--</div>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--@elseif(pathinfo($post->file_name, PATHINFO_EXTENSION) == 'mp4')--}}
                                                            {{--<div class="well" style="background-color: white">--}}

                                                                {{--<div class="embed-responsive embed-responsive-4by3">--}}
                                                                    {{--<iframe class="embed-responsive-item" src=""></iframe>--}}
                                                                {{--</div>--}}

                                                                {{--<div class="embed-responsive embed-responsive-16by9">--}}
                                                                    {{--<video autoplay loop class="embed-responsive-item" controls>--}}
                                                                        {{--<source src="/attachments/{{$post->file_name}}" type="video/mp4">--}}
                                                                    {{--</video>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--@else--}}
                                                            {{--<div class="well" style="background-color: white;">--}}
                                                                {{--<div class="media">--}}
                                                                    {{--<div class="media-left">--}}
                                                                        {{--<i class="fa fa-file-o fa-3x" style="background-color: white;"></i>--}}
                                                                    {{--</div>--}}
                                                                    {{--<div class="media-body">--}}
                                                                        {{--<a href="{{url('download', $post->id)}}">{{$post->file_name}}</a>--}}
                                                                    {{--</div>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--@endif--}}
                                                    {{--@else--}}

                                                    {{--@endif--}}

                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}

                                        {{--<div class="panel-footer">--}}
                                            {{--<div class="row">--}}
                                                {{--<div class="col-md-4">--}}
                                                    {{--@if($post->comments->count() > 0)--}}
                                                        {{--<h5 class="text-muted">{{$post->comments->count()}} Replies</h5>--}}
                                                    {{--@else--}}
                                                        {{--<h5>Reply</h5>--}}
                                                    {{--@endif--}}
                                                {{--</div>--}}
                                                {{--<div class="col-md-offset-8">--}}
                                                    {{--<h5>{{date("F d Y | g:i a", strtotime($post->created_at))}}</h5>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}

                                        {{--@if($post->comments->count() < 0)--}}
                                            {{--<div class="panel-footer">--}}
                                                {{--@foreach($post->comments as $comment)--}}
                                                    {{--<div class="row">--}}
                                                        {{--<div class="col-md-2">--}}
                                                            {{--<img src="/uploads/avatars/{{ $comment->user->avatar }}" alt="" style="width: 50px; height: 40px;" class="img img-responsive">--}}
                                                        {{--</div>--}}
                                                        {{--<div class="col-md-4    ">--}}
                                                            {{--<h6>{{$comment->user->first_name .' '.$comment->user->last_name}}</h6>--}}
                                                            {{--<h5 style="color: black;">{{$comment->comment}}</h5>--}}
                                                            {{--<h6 class="fa fa-circle-o"> {{$comment->created_at->diffForHumans()}}</h6>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<hr>--}}
                                                {{--@endforeach--}}
                                            {{--</div>--}}
                                        {{--@else--}}
                                        {{--@endif--}}

                                        {{--<div class="panel-footer">--}}
                                            {{--<form action="{{url('comment', $post->id)}}" method="post">--}}
                                                {{--{!! csrf_field() !!}--}}
                                                {{--<div class="form-horizontal">--}}
                                                    {{--<div class="form-group">--}}

                                                        {{--<div class="col-md-2">--}}
                                                            {{--<img src="/uploads/avatars/{{ Auth::user()->avatar }}" alt="" style="width: 50px; height: 40px;" class="img img-thumbnail">--}}
                                                        {{--</div>--}}

                                                        {{--<div class="col-md-10">--}}
                                                            {{--<input type="text" name="comment" class="form-control" placeholder="Type a reply...">--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}

                                                {{--<div class="form-horizontal">--}}
                                                    {{--<div class="form-group">--}}
                                                        {{--<div class="col-md-12">--}}
                                                            {{--<button type="submit" class="btn btn-primary pull-right">Reply</button>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</form>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                {{--@endif    --}}
                            {{--@endforeach--}}
                            {{----}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--Another One--}}
                    <div class="row">
                        <div class="col-md-7">
                            <div class="well bg bg-blue">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4><strong>Latest Posts</strong></h4>
                                    </div>

                                    <div class="col-md-3 pull-right">
                                        <div class="dropdown">
                                            <a type="button" data-toggle="dropdown" style="font-size: 13px; color: white; text-decoration: none;">Filter posts by
                                                <span class="fa fa-caret-down"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="#"><i class="fa fa-pencil-square-o"></i>Latest Posts</a></li>
                                                <li><a href="#"><i class="fa fa-pencil-square-o"></i>Latest Activity</a></li>
                                                <li><a href="#"><i class="fa fa-check-circle-o"></i>Assignments</a></li>
                                                <li><a href="#"><i class="fa fa-question-circle"></i>Quizzes</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                {{--<div class="panel-footer">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-md-4">--}}
                                            {{--@if($assignment->comments->count() < 0)--}}
                                                {{--<h5 class="text-muted">{{$assignment->comments->count()}} Replies</h5>--}}
                                            {{--@else--}}
                                                {{--<h5>Reply</h5>--}}
                                            {{--@endif--}}
                                        {{--</div>--}}
                                        {{--<div class="col-md-offset-8">--}}
                                            {{--<h5>{{date("F d Y | g:i a", strtotime($assignment->created_at))}}</h5>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                {{--@if($assignment->comments->count() > 0)--}}
                                    {{--<div class="panel-footer">--}}
                                        {{--@foreach($assignment->comments as $comment)--}}
                                            {{--<div class="row">--}}
                                                {{--<div class="col-md-2">--}}
                                                    {{--<img src="/uploads/avatars/{{ $comment->user->avatar }}" alt="" style="width: 50px; height: 40px;" class="img img-responsive">--}}
                                                {{--</div>--}}
                                                {{--<div class="col-md-4    ">--}}
                                                    {{--<h6>{{$comment->user->first_name .' '.$comment->user->last_name}}</h6>--}}
                                                    {{--<h5 style="color: black;">{{$comment->comment}}</h5>--}}
                                                    {{--<h6 class="fa fa-circle-o"> {{$comment->created_at->diffForHumans()}}</h6>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<hr>--}}
                                        {{--@endforeach--}}
                                    {{--</div>--}}
                                {{--@else--}}

                                {{--@endif--}}

                                {{--<div class="panel-footer">--}}
                                    {{--<form action="{{url('comment', $assignment->id)}}" method="post">--}}
                                        {{--{!! csrf_field() !!}--}}
                                        {{--<div class="form-horizontal">--}}
                                            {{--<div class="form-group">--}}

                                                {{--<div class="col-md-2">--}}
                                                    {{--<img src="/uploads/avatars/{{ Auth::user()->avatar }}" alt="" style="width: 50px; height: 40px;" class="img img-thumbnail">--}}
                                                {{--</div>--}}

                                                {{--<div class="col-md-10">--}}
                                                    {{--<input type="text" name="comment" class="form-control" placeholder="Type a reply...">--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}

                                        {{--<div class="form-horizontal">--}}
                                            {{--<div class="form-group">--}}
                                                {{--<div class="col-md-12">--}}
                                                    {{--<button type="submit" class="btn btn-primary pull-right">Reply</button>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</form>--}}
                                {{--</div>--}}

                            @foreach($group->assignments as $assignment)
                                <?php
                                $turns = 0;
                                foreach($turnin as $count):
                                    if($assignment->id == $count->assignment_id):
                                        $turns += 1;
                                    endif;
                                endforeach;
                                ?>

                                <div class="well">
                                    <div class="row">
                                        <div class="col-md-1">
                                            <img src="/uploads/avatars/{{ $assignment->user->avatar }}" alt="" style="width: 70px; height: 70px;">
                                        </div>

                                        <div class="col-md-9 col-md-offset-1">
                                            <p><a href="">{{$assignment->user->last_name}}</a> to <a href="">{{$group->group_name}}</a></p>
                                            <h5>{{$assignment->title}}</h5>

                                            <div class="well">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <a href="/assignment/{{$assignment->id}}/group/{{$group->id}}" role="button" class="btn btn-default text-primary">Turned In ({{$turns}})</a>
                                                    </div>

                                                    <div class="col-md-8 col-md-offset-2">
                                                        <p>Due Date:  {{date("F d Y", strtotime($assignment->due_date)) . " " . date("g:i a", strtotime($assignment->due_time))}}</p>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <hr>

                                                    <div class="col-md-12">
                                                        <p>{{$assignment->body}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        @if(Auth::user()->id == $assignment->user->id)
                                            <div class="dropdown col-md-1 pull-right">
                                                <a class="dropdown-toggle" type="button" data-toggle="dropdown">
                                                    <span class="caret"></span></a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#"><i class="fa fa-pencil"></i>Edit</a></li>
                                                    <li><a href="{{url('delete/assignment', $assignment->id)}}"><i class="fa fa-trash"></i>Delete</a></li>
                                                </ul>
                                            </div>
                                        @else
                                        @endif
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>


                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <div id="updatePost" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Update existing Post</h4>
                </div>
                <div class="modal-body">
                    <input type="text" name="body" value="" class="form-control">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>


    @include('sidebar.dashboard_control_sidebar')
    @include('group.partials._group_create')
    @include('group.partials._group_join')
    @include('group.partials._group_add_folders')
    @include('group.partials._group_add_members')


@endsection

@section('scripts')

    <script type="text/javascript" src="{{asset('js/select2.min.js')}}"></script>

    <script type="text/javascript">

        $(document).ready(function(){
            $(".nav-tabs a").click(function(){
                $(this).tab('show');
            });

            $(".posts").click(function () {
               $(this).show();
            });

            $(".folders").click(function () {
//               $(this).hide();
            });
        });


        $('.select2-multi').select2();
        {{--$('.select2-multi').select2().val({!! json_encode($post->groups()->getRelatedIds()) !!}).trigger('change');--}}
         $('.select2-multi').select2({
            placeholder: "Type the name of a group..."
        });


    </script>
    
@endsection