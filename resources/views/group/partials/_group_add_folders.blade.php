<!-- Modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="sharefolders">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            {{--<form action="{{url('folder', $group->id)}}" method="post">--}}

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Folders</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="#folder" data-toggle="pill">Folder</a></li>
                            </ul>
                        </div>


                        <div class="col-md-9">
                            <div class="tab-content">
                                <div id="folder" class="tab-pane fade">
                                    <form action="{{url('folder', $group->id)}}" method="post">
                                        <input type="text" name="folder_title" class="form-control" placeholder="Folder Title">
                                        <br><br><br><br><br>
                                        <hr>
                                        <input type="submit" value="Add Folder" class="btn btn-primary btn-block">
                                        <input type="hidden" value="{{ Session::token() }}" name="_token">
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix visible-lg"></div>
                    </div>
                </div>
            {{--</form>--}}
        </div>

    </div>
</div>


{{-- ---------------------------------}}
  {{----      END OF CREATE GROUP--}}
  {{---- ---------------------------------}}
  {{----}}

{{--<div class="modal fade" tabindex="-1" role="dialog" id="sharefolders">--}}
    {{--<div class="modal-dialog dialog-join" role="document">--}}
        {{--<div class="modal-content">--}}
            {{--<form action="#" method="post">--}}
                {{--{!! csrf_field() !!}--}}
                {{--<div class="modal-header">--}}
                    {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>--}}
                    {{--<h4 class="modal-title"><strong>Share folders with this Group</strong></h4>--}}
                {{--</div>--}}
                {{--<div class="modal-body modal-join">--}}
                    {{--<a href="#" role="button" class="btn btn-default text-primary">Add Folder</a>--}}
                {{--</div>--}}
                {{--<div class="modal-footer">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-md-6">--}}
                            {{--<button type="button" class="btn btn-default btn-block" data-dismiss="modal">Cancel</button>--}}
                        {{--</div>--}}

                        {{--<div class="col-md-6">--}}
                            {{--<button class="btn btn-primary pull-right btn-block" type="submit">Done</button>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</form>--}}
        {{--</div><!-- /.modal-content -->--}}
    {{--</div><!-- /.modal-dialog -->--}}
{{--</div><!-- /.modal -->--}}
