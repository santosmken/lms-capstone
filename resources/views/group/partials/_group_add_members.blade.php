<div id="addGroupMembers" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Group Members</h4>
            </div>
            <div class="modal-body">
                @foreach($group->users as $user)
                    <form action="{{url('add/group/members', $user->id)}}" method="post">
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-md-2">
                                <img src="/uploads/avatars/{{$user->avatar}}" class="media-object" style="width:40px; border-radius: 5px;">
                            </div>

                            <div class="col-md-6">
                                <h5>{{$user->first_name .' '. $user->last_name}}</h5>
                            </div>

                            <div class="col-md-1 col-md-offset-1">
                                <button type="submit" class="btn btn-info btn-sm">Add</button>
                            </div>

                        </div>
                    </form>
                @endforeach
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>