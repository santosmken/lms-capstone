{{-- -------------------------------
    --      CREATE  A GROUP
    -- -------------------------------
    --}}
<div class="modal fade" tabindex="-1" role="dialog" id="createGroupModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!-------------------------ROUTE group.create-------------------------->
            <form action="{{route('group.create')}}" method="POST">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Bring your classroom onto Edmoodology</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" name="group_name" placeholder="Name your group" class="form-control" disabled>
                        </div>
                    </div>

                    <br>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <select class="selectpicker" data-max-options="1" multiple title="College Year" name="college_year">
                                    <option>1st Year</option>
                                    <option>2nd Year</option>
                                    <option>3rd Year</option>
                                    <option>4th Year</option>
                                    <option>5th Year</option>
                                </select>
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <select class="selectpicker" data-max-options="1" multiple title="Section" name="section">
                                    <option>INF 131</option>
                                    <option>INF 132</option>
                                    <option>INF 133</option>
                                </select>
                            </div>
                        </div>


                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <select class="selectpicker" data-max-options="1" multiple title="Subject" name="subject">
                                    <option data-subtext="2">Capstone</option>
                                    <option>Data Communication</option>
                                    <option>Project Management</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <br>
                    By clicking "Create", I verify that I am an educator and that I am authorized to use Edmoodology with my students.

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    {{--<button type="button" class="btn btn-success">Create</button>--}}
                    {{--<input type="submit" value="Create" class="btn btn-success">--}}
                    <button class="btn btn-primary pull-right" type="submit">
                        Create
                    </button>

                    <input type="hidden" value="{{ csrf_token() }}" name="_token">
                </div>
            </form>
        </div><!-- /.modal-content -->


    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

{{-- -------------------------------
  --      END OF CREATE GROUP
  -- -------------------------------
  --}}