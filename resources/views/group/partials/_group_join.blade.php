{{-- -------------------------------
      --      JOIN A GROUP
      -- -------------------------------
      --}}
<div class="modal fade joinModal" tabindex="-1" role="dialog" id="joinGroupModal">
    <div class="modal-dialog dialog-join" role="document">
        <div class="modal-content">
            <form action="{{url('/join')}}" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><strong>Join Group</strong></h4>
                </div>
                <div class="modal-body modal-join">


                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" name="group_code" placeholder="Access Code" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    {{--<button type="button" class="btn btn-success">Join</button>--}}
                    <button class="btn btn-primary pull-right" type="submit">
                        Join
                    </button>

                    <input type="hidden" value="{{ Session::token() }}" name="_token">
                </div>

            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

{{-- -------------------------------
  --      END OF JOIN A GROUP
  -- -------------------------------
  --}}