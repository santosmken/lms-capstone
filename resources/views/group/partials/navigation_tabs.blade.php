<div class="panel panel-default">

    <div class="panel-body">
        <ul class="nav nav-tabs">
            <li><a href="#posts">Posts</a></li>
            <li class="active"><a href="#message">Message</a></li>
            <li><a href="#assignment">Assignment</a></li>
            <li><a href="#quiz">Quiz</a></li>
            <li class="pull-right"><a href="#members">Members <span class="label label-info">{{$group->users->count()}}</span></a></li>
            <li class="pull-right"><a href="#folders">Folders</a></li>
        </ul>

        <div class="tab-content">
            <div id="posts" class="tab-pane fade in active"><br>
                @foreach($group->assignments as $assignment)
                  <p>{{$assignment->body}}</p>
                @endforeach
            </div>

            <div id="message" class="tab-pane fade in active"><br>
                <form action="/groups/{{$group->id }}/posts"  method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <input type="text" name="body" placeholder="Type your message here..." class="form-control"><br>
                    <div class="row">
                        <div class="col-md-5">
                            <input type="file" name="file" style="margin-top: 5px;">
                        </div>
                        <div class="col-md-3 col-md-offset-4">
                            <button class="btn btn-primary pull-right" type="submit">Send</button>
                        </div>
                    </div>
                </form>
            </div>

            {{------------------------ASSIGNMENT---------------------}}

            <div id="assignment" class="tab-pane fade">
                <br>
                <form action="{{route('post.assignment', $group->id)}}"  method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                    {!! csrf_field() !!}

                    <input type="text" name="title" class="form-control" placeholder="Assignment Title"><br>
                    <input type="text" name="body" class="form-control" placeholder="Assignment Description"><br>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <input class="date form-control" type="text" name="due_date" placeholder="Due Date" data-date-start-date="0d">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" name="due_time" id="t1" value="{{date('g:i A')}}" data-format="hh:mm A" class="input-small form-control">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="lock"> Lock this assignment after its due date
                            </label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-5">
                            <input type="file" name="file" style="margin-top: 5px;">
                        </div>

                        <div class="col-md-3 col-md-offset-4">
                            <button class="btn btn-primary pull-right" type="submit">Send</button>

                        </div>
                    </div>
                </form>
            </div>


            {{--======================QUIZ======================--}}
            <div id="quiz" class="tab-pane fade"><br>
                <div class="row">
                    <div class="col-md-8">
                        <a href="{{url('quiz', $group->id)}}" role="button" type="submit" class="btn btn-facebook">Create a Quiz</a>
                    </div>
                </div>
            </div>

            {{--==================================================--}}

            <div id="members" class="tab-pane fade">
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-2">
                                <h4>Members</h4>
                            </div>
                        </div><hr>

                        @foreach($group->users as $user)
                            <div class="row">
                                <div class="col-md-2">
                                    <img src="/uploads/avatars/{{ $user->avatar }}" alt="" style="width: 70px; height: 70px;">
                                </div>

                                <div class="col-md-5">
                                    <h5><strong>{{$user->first_name . ' ' . $user->last_name}}</strong></h5>
                                    <h6>{{ucfirst($user->role)}}</h6>
                                </div>
                            </div>
                            <hr>
                        @endforeach

                    </div>
                </div>
            </div>

            {{--==================================================--}}

            <div id="folders" class="tab-pane fade">
                <br>
                <div class="row">
                    <div class="col-md-11">
                        <div class="row">
                            <div class="col-md-4">
                                <h4>Folders</h4>
                            </div>

                            <div class="col-md-3 col-md-push-5">
                                <a type="button" data-toggle="modal" data-target="#sharefolders" class="btn btn-primary" role="button">Manage Folders</a>
                            </div>
                        </div><br>

                        <div class="row">
                            <div class="col-md-3 col-md-offset-1">
                                <h6>Name</h6>
                            </div>

                            <div class="pull-right col-md-offset-1">
                                <h6>Modified Date</h6>
                            </div>
                        </div>

                        @foreach($group->folders as $folder)
                            <div class="row">
                                <div class="col-md-1">
                                    <i class="fa fa-folder-o fa-3x"></i>
                                </div>

                                <div class="col-md-6">
                                    <a href="/group/folder/{{$folder->id}}">{{$folder->folder_title}}</a>
                                    <h6>Owner: {{$folder->user->first_name . ' ' .$folder->user->last_name}}</h6>
                                </div>

                                <div class="pull-right col-md-offset-3">
                                    <br>
                                    <p>{{date('M d, Y', strtotime($folder->created_at))}}</p>
                                </div>
                            </div>
                        @endforeach
                        <hr>
                    </div>
                </div>
            </div>
        </div>

        <br>

    </div>
</div>