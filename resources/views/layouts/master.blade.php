<!doctype html>
<html lang="en">

    <head>
        @include('partials._head')

        @yield('stylesheets')

        @yield('css')
    </head>


    <body class="hold-transition skin-blue sidebar-mini">

        <div class="wrapper">

            @include('partials._nav')


            @yield('content')
            {{--TYPE OF USER--}}

        </div>

        {{--<div class="wrapper">--}}
            {{--@include('partials._footer')--}}
        {{--</div>--}}

        @include('partials._javascript')

        @yield('js')

        @yield('scripts');

    </body>
</html>