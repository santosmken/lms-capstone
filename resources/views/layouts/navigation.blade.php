<!doctype html>
<html lang="en">

<head>
    @include('partials._head')

    @yield('stylesheets')

    @yield('css')
</head>


<body class="hold-transition skin-yellow sidebar-mini">

    @include('partials._nav')


    @yield('content')
    {{--TYPE OF USER--}}



@include('partials._javascript')

@yield('js')

@yield('scripts');

</body>
</html>