@extends('layouts.master')

@section('title')
    Personal Locker - Professor
@endsection

@section('stylesheets')
    <link rel="stylesheet" href="{{asset("css/locker.css")}}">
@endsection

@section('content')


    @include('professor.dashboard_sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">

            <!-- Your Page Content Here -->
            <div class="content">
                <div class="row">

                    <div class="col-md-9">
                        <div class="panel panel-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <h4><strong>Locker</strong></h4>
                                </div>
                                <div class="col-md-3 col-md-offset-6">
                                    <a href="#" class="btn btn-primary btn-block" type="button" data-toggle="modal" data-target="#addItem">Add</a>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search Files">
                                        <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-2 col-md-offset-1">
                                    <h5>Name</h5>
                                </div>

                                <div class="col-md-3 col-md-offset-6">
                                    <a href=""><h5>Modified Date <i class="fa fa-caret-down"></i></h5></a>
                                </div>
                                <hr>
                            </div>

                            @foreach($user->files as $file)
                                    <div class="row">

                                        <div class="col-md-7">
                                            <a href="{{url('get/download', $file->id)}}">{{$file->file_name}}</a>
                                        </div>

                                        <div class="col-md-2 col-md-offset-2">
                                            <p>{{date('d-m-Y', strtotime($file->created_at))}}</p>

                                        </div>

                                        <div class="col-md-1 controls">
                                            <div class="btn-group pull-right">
                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                    <span class="fa fa-caret-down"></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="#"><i class="fa fa-repeat"></i>Edit</a></li>
                                                    <li><a href="#"><i class="fa fa-indent"></i>Move</a></li>
                                                    <li><a href="#"><i class="fa fa-copy"></i>Copy</a></li>
                                                    <li><a href="{{url('get/download', $file->id)}}"><i class="fa fa-download"></i>Download</a></li>
                                                    <li><a href="{{url('delete/file', $file->id)}}"><i class="fa fa-trash"></i>Delete</a></li>

                                                </ul>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                            @endforeach

                            <div class="row">
                                <div class="col-md-3">
                                    <h4><strong>Folders</strong></h4>
                                </div>
                            </div>

                            @foreach($user->folders as $folder)
                                <div class="row">

                                    <div class="col-md-5">
                                        <a href="{{url('folder', $folder->id)}}">{{$folder->folder_title}}</a>
                                    </div>

                                    <div class="col-md-2 col-md-offset-4">
                                        <p>{{date('d-m-Y', strtotime($folder->created_at))}}</p>

                                    </div>

                                    <div class="col-md-1">
                                        <div class="btn-group pull-right">
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                <span class="fa fa-caret-down"></span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#"><i class="fa fa-repeat"></i>Edit</a></li>
                                                <li><a href="#"><i class="fa fa-indent"></i>Move</a></li>
                                                <li><a href="#"><i class="fa fa-copy"></i>Copy</a></li>
                                                <li><a href=""><i class="fa fa-download"></i>Download</a></li>
                                                <li><a href="{{url('delete/folder', $folder->id)}}" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i>Delete</a></li>

                                            </ul>
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Modal -->
    <div id="addItem" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Item</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">
                            <ul class="nav nav-pills nav-stacked">
                                <li class="active"><a href="#file" data-toggle="pill">File</a></li>
                                <li><a href="#folder" data-toggle="pill">Folder</a></li>
                            </ul>
                        </div>


                        <div class="col-md-9">
                            <div class="tab-content">
                                <div id="file" class="tab-pane fade in active">
                                    <form action="{{url('add/file')}}" method="post" enctype="multipart/form-data">
                                        <input type="file" name="file" class="inputfile">
                                        <br><br><br><br><br><br><br>
                                        <hr>
                                        <input type="submit" value="Add File" class="btn btn-primary btn-block">
                                        <input type="hidden" value="{{ Session::token() }}" name="_token">
                                    </form>
                                </div>
                                <div id="folder" class="tab-pane fade">
                                    <form action="{{url('add/folder')}}" method="post">
                                        <input type="text" name="folder_title" class="form-control" placeholder="Folder Title">
                                        <br><br><br><br><br><br><br>
                                        <hr>
                                        <input type="submit" value="Add Folder" class="btn btn-primary btn-block">
                                        <input type="hidden" value="{{ Session::token() }}" name="_token">
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix visible-lg"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    @extends('sidebar.dashboard_control_sidebar')
    @include('group.partials._group_create')

@endsection

@section('scripts')

    <script type="text/javascript">

        $(document).ready(function () {
//            $('.controls').hide();
        });
    </script>
@endsection