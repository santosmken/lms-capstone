@extends('layouts.master')

@section('title')
    Profile Info
@endsection

@section('stylesheets')
    <link rel="stylesheet" href="{{asset("css/style.css")}}">
@endsection



@section('content')
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar user panel (optional) -->
            <div class="user-panel">
                <div class="pull-left info">
                    <br>
                    <p class="text-muted">Professor</p>
                    <!-- Status -->
                </div>
            </div>

            <!-- search form (Optional) -->
            <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
                </div>
            </form>
            <!-- /.search form -->

            <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <li class="header"><strong>MODULES</strong></li>
            {{--<li><a href="{{route('pages.locker')}}"><i class="fa fa-suitcase"></i> <span>Personal Locker</span></a></li>--}}
            <!-- Optionally, you can add icons to the links -->
                <li><a href="#"><i class="fa fa-user"></i> <span>Manage Profile</span></a></li>
                <li class="active"><a href="/change/password/{{Auth::user()->id}}"><i class="fa fa-key"></i> <span>Change Password</span></a></li>

            </ul>
            <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">

            <!-- Your Page Content Here -->
            <div class="content">
                <div class="row">

                    <div class="col-md-8">
                        @include('includes.message-block')

                        <div class="well" style="background-color: white">
                            <form action="{{url('change/password', $user->id)}}" method="post">
                                {!! csrf_field() !!}
                                <h4>Password</h4>
                                <hr style="border: 1px solid gainsboro">

                                <label for="current_password">Current Password</label>
                                <input type="text" name="current_password" id="current_password" class="form-control"><br>

                                <label for="new_password">New Password</label>
                                <input type="text" name="new_password" id="new_password" class="form-control"><br>

                                <label for="new_password_confirmation">Confirm Password</label>
                                <input type="text" name="new_password_confirmation" id="new_password_confirmation" class="form-control">
                                <br>
                                <button type="submit" class="btn btn-primary">Change Password</button>
                            </form>
                        </div>
                    </div>
                </div>


            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->


@endsection