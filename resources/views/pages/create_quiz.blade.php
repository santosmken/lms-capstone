@extends('layouts.navigation')

@section('content')


    <div class="container">
        <br>
        <div class="row">
            <div class="col-md-12">

                <div class="row ">
                    <form action="/quiz/{{$group->id}}" method="post">
                        {!! csrf_field() !!}
                        <div class="col-md-9">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="text" name="title" class="form-control" value="Untitled Quiz - {{date('Y-m-d-g-i-a')}}">
                                        </div>

                                        <div class="col-md-2">
                                            <h5><strong>Time Limit:</strong></h5>
                                        </div>

                                        <div class="col-md-2">
                                            <input type="text" name="time_limit" class="form-control" value="60">
                                        </div>

                                        <div class="col-md-2">
                                            <h5>Minutes</h5>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-2">

                                        </div>

                                        <div class="col-md-10">
                                            <h4><strong>Add question to start a quiz..</strong></h4><hr>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <div class="form-inline">
                                                        <label for="type">Type </label>
                                                        <select class="selectpicker show-tick" name="type" id="type">
                                                            <option value="mc">Multiple Choice</option>
                                                            <option value="tf">True False</option>
                                                            <option>Fill in the blank</option>
                                                            <option>Matching type</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <button type="submit" class="btn btn-default btn-warning"><i class="fa fa-plus"></i> Add First Question</button>
                                                </div>
                                            </div>
                                            <hr>

                                            <div class="col-md-4 col-md-offset-2">
                                                <i class="fa fa-question-circle fa-5x"></i>
                                            </div>

                                            <div class="col-md-5 col-md-pull-1">
                                                <h5><strong>Quiz Help</strong></h5>
                                                <p>Changes made to the quiz will automatically save. You can assign or
                                                    edit this quiz at a later time by loading it from the Post Box on
                                                    the Home page. </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                    {{--<div class="col-md-3">--}}
                        {{--<div class="row">--}}
                            {{--<form action="" method="post">--}}
                                {{--<div class="panel panel-default">--}}
                                    {{--<div class="panel-heading">--}}
                                        {{--<button type="submit" class="btn btn-primary btn-block">Done</button>--}}
                                        {{--<br>--}}
                                        {{--<div class="row">--}}
                                            {{--<div class="col-md-8 col-md-offset-3">--}}
                                                {{--<a href="" style="text-decoration: none"><span class="glyphicon glyphicon-print"></span> Print Quiz</a>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="panel-body">--}}
                                        {{--<hr>--}}
                                        {{--<div class="form">--}}
                                            {{--<div class="form-group">--}}
                                                {{--<label for="about_quiz">About this Quiz</label>--}}
                                                {{--<textarea name="quiz_about" id="about_quiz" cols="10" rows="3" class="form-control"></textarea>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}

                                        {{--<hr>--}}
                                        {{--<hr>--}}

                                        {{--<h5><strong>Quiz Options</strong></h5>--}}

                                        {{--<div class="checkbox">--}}
                                            {{--<label><input type="checkbox" value=""> Show results</label>--}}
                                        {{--</div>--}}

                                        {{--<div class="checkbox">--}}
                                            {{--<label><input type="checkbox" value=""> Randomize questions</label>--}}
                                        {{--</div>--}}

                                    {{--</div>--}}
                                {{--</div>--}}

                            {{--</form>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
    </div>
@endsection