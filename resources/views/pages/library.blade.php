@extends('layouts.master')

@section('title')
    Personal Locker
@endsection

@section('stylesheets')
    <link rel="stylesheet" href="{{asset("css/locker.css")}}">
@endsection


@section('content')

    <div class="laradrop" laradrop-csrf-token="{{ csrf_token() }}"> </div>

    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar user panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="/uploads/avatars/{{ Auth::user()->avatar }}" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <h5 class="text-muted text-center"><strong>Hello, {{$user->last_name}}</strong></h5>
                    <h6 class="text-muted">Professor</h6>
                    <!-- Status -->
                </div>
            </div>
            <br>
            <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <li class="header"><strong>File Storage</strong></li>
                <!-- Optionally, you can add icons to the links -->
                <li class="active"><a href="#"><i class="fa fa-server"></i> Personal Locker</a></li>
                <li><a href="#"><i class="fa fa-cloud"></i> OneDrive</a></li>
                
            </ul>
            <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>



    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">

            <!-- Your Page Content Here -->
            <div class="content">
                <div class="row">
                    <div class="col-md-8">
                        <div class="panel panel-default">

                            <div class="panel-body">
                                <br>
                                <div class="row">
                                    <div class="col-md-4">
                                        <h4><strong>Locker</strong></h4>
                                    </div>

                                    <div class="col-md-2 col-md-offset-6">
                                        <a type="button" data-toggle="modal" data-target="#addItem" class="btn btn-primary btn-block">Add</a>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-4 col-md-offset-1">Name</div>

                                    <div class="col-md-4 col-md-offset-3">Modified Date <i class="fa fa-caret-up"></i></div>
                                </div>
                                <hr>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>



    <!--------------------------------------- Control Sidebar ----------------------------------------->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
            <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
            <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- Home tab content -->
            <div class="tab-pane active" id="control-sidebar-home-tab">
                <h3 class="control-sidebar-heading">Recent Activity</h3>
                <ul class="control-sidebar-menu">
                    <li>
                        <a href="javascript::;">
                            <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                                <p>Will be 23 on April 24th</p>
                            </div>
                        </a>
                    </li>
                </ul>
                <!-- /.control-sidebar-menu -->

                <h3 class="control-sidebar-heading">Tasks Progress</h3>
                <ul class="control-sidebar-menu">
                    <li>
                        <a href="javascript::;">
                            <h4 class="control-sidebar-subheading">
                                Custom Template Design
                                <span class="label label-danger pull-right">70%</span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                            </div>
                        </a>
                    </li>
                </ul>
                <!-- /.control-sidebar-menu -->

            </div>
            <!-- /.tab-pane -->
            <!-- Stats tab content -->
            <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
            <!-- /.tab-pane -->
            <!-- Settings tab content -->
            <div class="tab-pane" id="control-sidebar-settings-tab">
                <form method="post">
                    <h3 class="control-sidebar-heading">General Settings</h3>

                    <div class="form-group">
                        <label class="control-sidebar-subheading">
                            Report panel usage
                            <input type="checkbox" class="pull-right" checked>
                        </label>

                        <p>
                            Some information about this general settings option
                        </p>
                    </div>
                    <!-- /.form-group -->
                </form>
            </div>
            <!-- /.tab-pane -->
        </div>
    </aside>
    <!--------------------------------------- END of Control Sidebar ----------------------------------------->


    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>

    {{-- -------------------------------
    --      ATTACHMENTS
    -- -------------------------------
    --}}
    <div class="modal fade addItem" tabindex="-1" role="dialog" id="addItem">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <form action="#" method="POST">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Add Item</h4>
                    </div>

                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <ul class="list-group">
                                    <li class="list-group-item"><a href="#file">File</a></li>
                                    <li class="list-group-item"><a href="#folder">Folder</a></li>
                                    <li class="list-group-item"><a href="#quiz">Quiz</a></li>

                                </ul>
                                <br><br><br><br><br><br>
                            </div>

                            <div class="col-md-8">
                                <div class="tab-content">
                                    <div id="file" class="tab-pane fade in active">
                                        <a href="{!! url('filemanager/index.html') !!}" type="button" class="btn btn-default btn-block text-primary">Choose Files</a>
                                        <br><br><br><br><br><br><br><br><hr>
                                        <button class="btn btn-primary btn-block" type="submit">
                                            Add File
                                        </button>

                                        <input type="hidden" value="{{ Session::token() }}" name="_token">
                                    </div>
                                    <div id="folder" class="tab-pane fade">
                                        <input type="text" name="foldername" id="foldername" class="form-control" placeholder="Folder Title">
                                        <br><br><br><br><br><br><br><hr>
                                        <button type="submit" class="btn btn-primary btn-block">Add Folder</button>
                                        <input type="hidden" value="{{ Session::token() }}" name="_token">
                                    </div>
                                    <div id="quiz" class="tab-pane fade">
                                            <h4 class="text-center"><strong>Create Your Quiz Below</strong></h4>
                                            <br><br>
                                            <h5 class="text-center">After you save your quiz it will then show up
                                                in the 'My Quizzes' folder</h5>
                                            <br><br><br><br><br><hr>
                                            <button type="submit" class="btn btn-primary btn-block">Create Quiz</button>
                                            <input type="hidden" value="{{ Session::token() }}" name="_token">

                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>


                </form>
            </div><!-- /.modal-content -->


        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    {{-- -------------------------------
      --      END OF ATTACHMENTS
      -- -------------------------------
      --}}

@endsection

@section('scripts')



    <script style="text/javascript">

        $(document).ready(function(){
            $(".list-group a").click(function(){
                $(this).tab('show');
            });
        });

        jQuery(document).ready(function(){
            jQuery('.laradrop').laradrop();
        });

    </script>

@endsection
