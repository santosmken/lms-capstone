@extends('layouts.master')

@section('title')
    Personal Locker
@endsection

@section('content')


    @include('professor.dashboard_sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">

            <!-- Your Page Content Here -->
            <div class="content">
                <div class="row">

                    <div class="col-md-8">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-md-2">
                                        <h3>Locker</h3>
                                    </div>
                                    <div class="col-md-2 col-md-offset-8">
                                        {{--<button type="button" class="btn btn-primary btn-md editor" data-toggle="modal" data-target="#myFile" style="margin: 10px; ">Add</button>--}}


                                    </div>
                                </div>
                                <div class="row">
                                   <div class="col-md-6">
                                       <div class="input-group">
                                           <input type="text" class="form-control" placeholder="Search File's" aria-describedby="basic-addon2">
                                           <span class="input-group-addon" id="basic-addon2"><i class="fa fa-search"></i></span>
                                       </div>
                                   </div>
                                </div>
                                <br>

                                <div class="row">
                                    <div class="input-group">
                                      <span class="input-group-btn">
                                          {{--
                                          pakita m muna sakin bro san part nag error ng ma debug natin--}}
                                        <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                          <i class="fa fa-picture-o"></i> Choose
                                        </a>

                                          {{--YAN PART ata nayan bro hindi niya makita ung route--}}
                                      </span>
                                        <input id="thumbnail" class="form-control" type="text" name="filepath">
                                    </div>
                                    <img id="holder" style="margin-top:15px;max-height:100px;">
                                    {{--@foreach($files as $file)--}}
                                        <div class="col-md-3">
                                            <h5>Name</h5>
                                            {{--<p>{{$file->filename}}</p>--}}
                                        </div>

                                        <div class="col-md-3 col-md-offset-6">
                                            <h5>Modified Date</h5>
                                            {{--<p>{{$file->created_at}}</p>--}}
                                        </div>
                                        <hr>
                                    {{--@endforeach--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    @include('sidebar.dashboard_control_sidebar')

    {{--@include('modal.file_manager')--}}

@endsection

@section('scripts')

    <script src="/vendor/laravel-filemanager/js/lfm.js"></script>
    {{--file upload lang ba gagawin mo ? --}}
    {{--oo pre pakac--}}
    <script type="text/javascript">
        $('#lfm').filemanager('file');
    </script>
    {{--san error mo dito bro? post ka nga bro sample--}}

    {{--DI KO MAOPEN UNG FILE MANAGER E--}}
    {{--FILE MANAGER LANG BRO UNG NEED KO--}}
@endsection