@extends('layouts.master')

@section('title')
    Profile Info
@endsection

@section('stylesheets')
    <link rel="stylesheet" href="{{asset("css/style.css")}}">
@endsection



@section('content')
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar user panel (optional) -->
            <div class="user-panel">
                <div class="pull-left info">
                    <br>
                    <p class="text-muted">Professor</p>
                    <!-- Status -->
                </div>
            </div>

            <!-- search form (Optional) -->
            <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
                </div>
            </form>
            <!-- /.search form -->

            <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <li class="header"><strong>MODULES</strong></li>
                {{--<li><a href="{{route('pages.locker')}}"><i class="fa fa-suitcase"></i> <span>Personal Locker</span></a></li>--}}
                <!-- Optionally, you can add icons to the links -->
                <li class="active"><a href="#"><i class="fa fa-user"></i> <span>Manage Profile</span></a></li>
                <li><a href="/change/password/{{Auth::user()->id}}"><i class="fa fa-key"></i> <span>Change Password</span></a></li>

            </ul>
            <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">

            <!-- Your Page Content Here -->
            <div class="content">
                <div class="row">

                    <div class="col-md-12">
                        @include('includes.message-block')
                        <form enctype="multipart/form-data" action="/profile" method="post">
                            <div class="row">

                                <div>
                                    <h3><strong>Personal Information</strong></h3>
                                </div>

                                <div class="col-md-3">
                                    <img src="/uploads/avatars/{{ $user->avatar }}" style="width:200px; height:200px; float:left; margin-right: 25px;" class="img img-thumbnail">

                                    <br>
                                    <label for="profile" style="margin-left: 30px;">Update Profile Image</label>

                                    <input type="file" name="avatar">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                </div>

                                <div class="col-md-9">
                                        {{--<h3>{{ $user->last_name . ", " .$user->first_name }}</h3>--}}
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="secondary_email">Primary Email</label>
                                            <input type="text" name="secondary_email" id="secondary_email" class="form-control" placeholder="{{$user->email}}" disabled>
                                        </div>

                                        <div class="col-md-6">
                                            <label for="secondary_email">Secondary Email</label>
                                            <input type="email" name="secondary_email" id="secondary_email" class="form-control"
                                                   @if((!empty(Auth::user()->secondary_email)))
                                                   value="{{Auth::user()->secondary_email}}">
                                                   @else
                                                       value="">
                                                   @endif

                                        </div>

                                        </div>
                                        <br><br>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label for="title">Title</label>
                                            <div class="form-group">
                                                <select class="selectpicker" data-max-options="1" name="title" id="title" data-width="fit">
                                                    <option>Mr.</option>
                                                    <option>Mrs.</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-5">
                                            <label for="first_name">First Name</label>
                                            <input type="text" name="first_name" id="first_name" class="form-control" placeholder="{{$user->first_name}}" disabled>
                                        </div>

                                        <div class="col-md-5">
                                            <label for="last_name">Last Name</label>
                                            <input type="text" name="last_name" id="last_name" class="form-control" placeholder="{{$user->last_name}}" disabled>

                                            <br><br>
                                        </div>
                                    </div>
                                    <br><br>
                                    <div class="row">
                                        <div class="col-md-4 pull-right">
                                            <input type="submit" value="Save Personal Info" class="btn btn-primary btn-group">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br><br>
                        </form>
                    </div>
                </div>
                {{--END OF ROW--}}

                <div class="row">
                    <h4><strong>Connect Your Other Accounts</strong></h4>
                    <hr style="border: 1px solid lightgrey;">
                    <div class="col-sm-3">
                        <a class="btn btn-primary" href="{{ url('auth/facebook') }}">
                            <i class="fa fa-facebook"></i> Connect with Facebook
                        </a>
                    </div>

                    <div class="col-sm-3">
                        <h6>Share content with your friends on Facebook</h6>
                    </div>
                </div>
            </div>
            {{--END OF CONTENT--}}




        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->


@endsection