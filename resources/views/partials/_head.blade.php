<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>@yield('title')</title>

<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">


<!-- Ionicons -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
{{--Admin LTE--}}
<link rel="stylesheet" href="{{asset("css/AdminLTE.min.css")}}">

{{--Bootstrap--}}
<link rel="stylesheet" href="{{asset("css/bootstrap.min.css")}}">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">

<link rel="stylesheet" href="{{asset('css/clockface.css')}}">


<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.0/css/bootstrap-select.min.css">
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">
{{--Font Awesome--}}
<link rel="stylesheet" href="{{asset("css/font-awesome.min.css")}}">


<link rel="stylesheet" href="{{asset("css/skins/skin-yellow.min.css")}}">
<link rel="stylesheet" href="{{asset("css/skins/skin-blue.min.css")}}">


{{--Main CSS--}}
<link rel="stylesheet" href="{{ asset ('css/main.css') }}">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->


    
