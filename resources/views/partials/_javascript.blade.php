<script src="{{asset("js/jquery.js")}}"></script>
<script src="{{asset("js/moment.js")}}"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.min.js" ></script>
<script src="{{asset("js/bootstrap.min.js")}}"></script>
<script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>

<script src="{{asset('js/clockface.js')}}"></script>
<script src="{{asset("js/app.min.js")}}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twbs-pagination/1.3.1/jquery.twbsPagination.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.0/js/bootstrap-select.min.js"></script>



<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>
<script src="{{asset('js/custom.js')}}"></script>
