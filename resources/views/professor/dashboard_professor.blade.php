@extends('layouts.master')

@section('title')
    Welcome To Professor Page
@endsection

@section('stylesheets')
    <link rel="stylesheet" href="{{asset("css/style.css")}}">
    <link rel="stylesheet" href="{{asset("css/select2.min.css")}}">
    <link rel="stylesheet" href="{{asset("css/bootstrap-timepicker.min.css")}}">

@endsection

@section('content')

    @include('professor.dashboard_sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">

            <!-- Your Page Content Here -->
            <div class="content">

                <div class="row">
                    <div class="col-md-7">
                        @include('includes.message-block')
                        @include('professor.nav-tabs.tabs')
                    </div>
                </div>


                        {{--@foreach($assignment->turnins as $turnin)--}}
                            {{--<li class="list-group-item">--}}
                                {{--@foreach($group->users as $user)--}}
                                    {{--<a href="{{url('grading', $turnin->id .'/'.$group->id . '/'. $assignment->id)}}">--}}
                                        {{--<strong>--}}
                                            {{--{{$user->id == $turnin->user_id ? "" .$user->first_name . " " .$user->last_name : ""}}--}}
                                        {{--</strong>--}}
                                    {{--</a>--}}
                                {{--@endforeach--}}
                            {{--</li>--}}
                        {{--@endforeach--}}

                <div class="row">
                    <div class="col-md-7">
                        <div class="well bg bg-blue-gradient">
                            <h4><strong>Latest Posts</strong></h4>
                        </div>

                        @foreach($user->groups as $group)
                            @foreach($group->posts as $post)
                                {{--<div class="well">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-md-1">--}}
                                            {{--<img src="/uploads/avatars/{{ $post->user->avatar }}" style="width: 50px; height: 50px;">--}}
                                        {{--</div>--}}

                                        {{--<div class="col-md-9 col-md-offset-1">--}}
                                            {{--<p><a href="">{{$post->user->last_name}}</a> to <a href="">{{$group->group_name}}</a></p>--}}
                                            {{--{{$post->body}}--}}
                                            {{--<h6>{{date('D M d,Y | g:i a', strtotime($post->created_at))}}</h6>--}}
                                        {{--</div>--}}

                                        {{--@if(Auth::user()->id == $post->user->id)--}}
                                            {{--<div class="dropdown col-md-1 pull-right">--}}
                                                {{--<a class="dropdown-toggle" type="button" data-toggle="dropdown">--}}
                                                    {{--<span class="caret"></span></a>--}}
                                                {{--<ul class="dropdown-menu">--}}
                                                    {{--<li><a href="#"><i class="fa fa-pencil"></i>Edit</a></li>--}}
                                                    {{--<li><a href="{{url('delete/post', $post->id)}}"><i class="fa fa-trash"></i>Delete</a></li>--}}
                                                {{--</ul>--}}
                                            {{--</div>--}}
                                        {{--@else--}}
                                        {{--@endif--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            @endforeach

                            @foreach($group->assignments as $assignment)
                            @endforeach
                            @if($post->created_at < $assignment->created_at)

                                @foreach($group->posts as $post)
                                        <div class="well">
                                            <div class="row">
                                                <div class="col-md-1">
                                                    <img src="/uploads/avatars/{{ $post->user->avatar }}" style="width: 50px; height: 50px;">
                                                </div>

                                                <div class="col-md-9 col-md-offset-1">
                                                    <p><a href="">{{$post->user->last_name}}</a> to <a href="">{{$group->group_name}}</a></p>
                                                    {{$post->body}}
                                                    <h6>{{date('D M d,Y | g:i a', strtotime($post->created_at))}}</h6>
                                                </div>

                                                @if(Auth::user()->id == $post->user->id)
                                                    <div class="dropdown col-md-1 pull-right">
                                                        <a class="dropdown-toggle" type="button" data-toggle="dropdown">
                                                            <span class="caret"></span></a>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="#"><i class="fa fa-pencil"></i>Edit</a></li>
                                                            <li><a href="{{url('delete/post', $post->id)}}"><i class="fa fa-trash"></i>Delete</a></li>
                                                        </ul>
                                                    </div>
                                                @else
                                                @endif
                                            </div>
                                        </div>
                                @endforeach

                                    @foreach($group->assignments as $assignment)
                                        @foreach($group->assignments as $assignment)
                                            <?php
                                            $turns = 0;
                                            foreach($turnin as $count):
                                                if($assignment->id == $count->assignment_id):
                                                    $turns += 1;
                                                endif;
                                            endforeach;
                                            ?>

                                            <div class="well">
                                                <div class="row">
                                                    <div class="col-md-1">
                                                        <img src="/uploads/avatars/{{ $assignment->user->avatar }}" alt="" style="width: 70px; height: 70px;">
                                                    </div>

                                                    <div class="col-md-9 col-md-offset-1">
                                                        <p><a href="">{{$assignment->user->last_name}}</a> to <a href="">{{$group->group_name}}</a></p>
                                                        <h5>{{$assignment->title}}</h5>

                                                        <div class="well">
                                                            <div class="row">
                                                                <div class="col-md-2">
                                                                    <a href="/assignment/{{$assignment->id}}/group/{{$group->id}}" role="button" class="btn btn-default text-primary">Turned In ({{$turns}})</a>
                                                                </div>

                                                                <div class="col-md-8 col-md-offset-2">
                                                                    <p>Due Date:  {{date("F d Y", strtotime($assignment->due_date)) . " " . date("g:i a", strtotime($assignment->due_time))}}</p>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <hr>

                                                                <div class="col-md-12">
                                                                    <p>{{$assignment->body}}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    @if(Auth::user()->id == $assignment->user->id)
                                                        <div class="dropdown col-md-1 pull-right">
                                                            <a class="dropdown-toggle" type="button" data-toggle="dropdown">
                                                                <span class="caret"></span></a>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="#"><i class="fa fa-pencil"></i>Edit</a></li>
                                                                <li><a href="{{url('delete/assignment', $assignment->id)}}"><i class="fa fa-trash"></i>Delete</a></li>
                                                            </ul>
                                                        </div>
                                                    @else
                                                    @endif
                                                </div>
                                            </div>
                                        @endforeach
                                    @endforeach


                            @elseif($assignment->created_at < $post->created_at)
                                    @foreach($group->assignments as $assignment)
                                        @foreach($group->assignments as $assignment)
                                            <?php
                                            $turns = 0;
                                            foreach($turnin as $count):
                                                if($assignment->id == $count->assignment_id):
                                                    $turns += 1;
                                                endif;
                                            endforeach;
                                            ?>

                                            <div class="well">
                                                <div class="row">
                                                    <div class="col-md-1">
                                                        <img src="/uploads/avatars/{{ $assignment->user->avatar }}" alt="" style="width: 70px; height: 70px;">
                                                    </div>

                                                    <div class="col-md-9 col-md-offset-1">
                                                        <p><a href="">{{$assignment->user->last_name}}</a> to <a href="">{{$group->group_name}}</a></p>
                                                        <h5>{{$assignment->title}}</h5>

                                                        <div class="well">
                                                            <div class="row">
                                                                <div class="col-md-2">
                                                                    <a href="/assignment/{{$assignment->id}}/group/{{$group->id}}" role="button" class="btn btn-default text-primary">Turned In ({{$turns}})</a>
                                                                </div>

                                                                <div class="col-md-8 col-md-offset-2">
                                                                    <p>Due Date:  {{date("F d Y", strtotime($assignment->due_date)) . " " . date("g:i a", strtotime($assignment->due_time))}}</p>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <hr>

                                                                <div class="col-md-12">
                                                                    <p>{{$assignment->body}}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    @if(Auth::user()->id == $assignment->user->id)
                                                        <div class="dropdown col-md-1 pull-right">
                                                            <a class="dropdown-toggle" type="button" data-toggle="dropdown">
                                                                <span class="caret"></span></a>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="#"><i class="fa fa-pencil"></i>Edit</a></li>
                                                                <li><a href="{{url('delete/assignment', $assignment->id)}}"><i class="fa fa-trash"></i>Delete</a></li>
                                                            </ul>
                                                        </div>
                                                    @else
                                                    @endif
                                                </div>
                                            </div>
                                        @endforeach
                                    @endforeach

                                        @foreach($group->posts as $post)
                                            <div class="well">
                                                <div class="row">
                                                    <div class="col-md-1">
                                                        <img src="/uploads/avatars/{{ $post->user->avatar }}" style="width: 50px; height: 50px;">
                                                    </div>

                                                    <div class="col-md-9 col-md-offset-1">
                                                        <p><a href="">{{$post->user->last_name}}</a> to <a href="">{{$group->group_name}}</a></p>
                                                        {{$post->body}}
                                                        <h6>{{date('D M d,Y | g:i a', strtotime($post->created_at))}}</h6>
                                                    </div>

                                                    @if(Auth::user()->id == $post->user->id)
                                                        <div class="dropdown col-md-1 pull-right">
                                                            <a class="dropdown-toggle" type="button" data-toggle="dropdown">
                                                                <span class="caret"></span></a>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="#"><i class="fa fa-pencil"></i>Edit</a></li>
                                                                <li><a href="{{url('delete/post', $post->id)}}"><i class="fa fa-trash"></i>Delete</a></li>
                                                            </ul>
                                                        </div>
                                                    @else
                                                    @endif
                                                </div>
                                            </div>
                                        @endforeach
                            @endif
                        @endforeach
                    </div>
                </div>

            </div>


        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    @include('sidebar.dashboard_control_sidebar')


    {{-- ----------------------------------------------------
     --                 EDIT CONTENT(s) MODAL
     -- -----------------------------------------------------
     --}}
    <div class="modal fade" tabindex="-1" role="dialog" id="edit-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                {{--<form method="post" action={{route('post.edit', ['id'=>$user->id])}}>--}}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Post</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label for="post-body">Edit the Post</label>
                        <textarea class="form-control" name="body" id="body" rows="5"></textarea>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    <input type="submit" value="Update" name="update" class="btn btn-primary">
                    <input type="hidden" value="{{ Session::token() }}" name="_token">

                    {{--<button type="button" class="btn btn-primary" id="modal-save">Save changes</button>--}}
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    {{-- ----------------------------------------------------
     --                 EDIT POST'S CONTENT(s)
     -- -----------------------------------------------------
     --}}

    @include('group.partials._group_create')
    @include('group.partials._group_join')

@endsection

@section('scripts')

    <script type="text/javascript" src="{{asset('js/select2.min.js')}}"></script>
    <script type="text/javascript">

        $(document).ready(function(){
            $(".nav-tabs a").click(function(){
                $(this).tab('show');
            });
        });

        $('.select2-multi').select2({
            placeholder: "Type the name of a group...",
            tags: true,
        });

        $('.select2-multi-assignment').select2({
           placeholder: "Send to..."
        });


    </script>

@endsection


