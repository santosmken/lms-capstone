<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/uploads/avatars/{{ Auth::user()->avatar }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <h5 class="text-muted text-center"><strong>Hello, {{Auth::user()->last_name}}</strong></h5>
                <h6 class="text-muted">{{ucfirst(Auth::user()->role)}}</h6>
                <!-- Status -->
            </div>
        </div>


        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header"><strong>MODULES</strong></li>
            <!-- Optionally, you can add icons to the links -->
            <li class="active"><a href="#"><i class="fa fa-calendar"></i> <span>Calendar</span></a></li>

            <li><a href="{{url('personal/locker')}}"><i class="fa fa-suitcase"></i> <span>Personal Locker</span></a></li>
            <li><a href="{{url('/forums')}}"><i class="fa fa-stack-exchange"></i> <span>Stack Exchange</span></a></li>
            <li class="treeview">
                <a href="#"><i class="fa fa-users"></i> <span>Groups</span> <i class="fa fa-angle-down pull-right"></i></a>
                <ul class="treeview-menu">
                    <!-------------------- GROUP'S CREATED--------------------->
                    @foreach($user->groups as $group)
                        <li><a href="{{route('group.get', $group->id)}}">{{$group->group_name}}</a></li>
                    @endforeach
                <!---------------------------------------------------------->
                </ul>
            </li>

            <li><a href="{{route('manage.group')}}"><i class="fa fa-gear"></i> <span>Manage Groups</span></a></li>

            <li><a type="button" data-toggle="modal" data-target="#createGroupModal">
                    <i class="fa fa-plus-circle"></i> <span>Create a Group</span></a></li>

            <li><a type="button" data-toggle="modal" data-target="#joinGroupModal">
                    <i class="fa fa-users"></i> <span>Join a Group</span></a></li>

            <li><a type="button" data-toggle="modal" data-target="#addGroupMembers"><i class="fa fa-user"></i> <span>Add Members</span></a></li>

            <li class="treeview">
                <a href="#"><i class="fa fa-line-chart"></i> <span>Progress</span> <i class="fa fa-angle-down pull-right"></i></a>
                <ul class="treeview-menu">
                    <!-------------------- GROUP'S CREATED--------------------->
                    @foreach($user->groups as $group)
                        <li><a href="{{route('group.get', $group->id)}}">{{$group->group_name}}</a></li>
                    @endforeach
                    <!---------------------------------------------------------->
                </ul>
            </li>

        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>