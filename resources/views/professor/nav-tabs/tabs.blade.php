<div class="well">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#home">Message</a></li>

        <li><a href="#menu1">Assignment</a></li>
        <li><a href="#menu2">Quiz</a></li>
        <li><a href="#menu3">Poll</a></li>

    </ul>

    {{--ALL NAV TABS--}}
    <div class="tab-content">

        <div id="home" class="tab-pane fade in active">
            <br>
            <form action="{{url('home/post')}}"  method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <input type="text" name="body" placeholder="Type your message here..." class="form-control">
                <br>

                <select class="form-control select2-multi" name="groups[]" multiple="multiple" style=" width: 100%">
                    @foreach($user->groups as $group)
                        <option value="{{$group->id}}">{{$group->group_name}}</option>
                    @endforeach
                </select>

                <br><br>
                <div class="btn-group" role="group" aria-label="...">
                    <input type="file" name="file" style="margin-top: 5px;">
                </div>

                <button class="btn btn-primary pull-right" type="submit">Send</button>
            </form>
        </div>


        {{--==================================================--}}

        <div id="menu1" class="tab-pane fade">
            <br>
            <form action="{{url('home/assignment')}}"  method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <input type="text" name="title" class="form-control" placeholder="Assignment Title">
                <br>
                <div class="row">

                    <div class="col-md-6">
                        {{--<input type="date" name="due_date" id="published_at" class="form-control">--}}
                        <div class="input-group">
                            <input class="date form-control" type="text" name="due_date" placeholder="Due Date" data-date-start-date="0d">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="input-group">
                            {{--<input id="timepicker1" type="time" class="form-control input-small" name="due_time">--}}
                            <input type="text" name="due_time" id="t1" value="{{date('g:i A')}}" data-format="hh:mm A" class="input-small form-control">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <br>
                        <input type="text" name="body" class="form-control" placeholder="Assignment Description">
                        <br>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <select class="form-control select2-multi-assignment" name="groups[]" multiple="multiple" style="width: 100%">
                            @foreach($user->groups as $group)
                                <option value="{{$group->id}}">{{$group->group_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <br>
                <div class="form-group">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value="1" name="lock"> Lock this assignment after its due date
                        </label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3">
                        <input type="file" name="file">
                    </div>

                    <div class="col-md-2 col-md-offset-7">
                        <button class="btn btn-primary pull-right" type="submit">Send</button>
                    </div>
                </div>

            </form>
        </div>


        {{--==================================================--}}

        <div id="menu2" class="tab-pane fade">
            <br>
            <div class="row">
                <div class="col-md-8">
                    <a href="{{url('create/quiz')}}"  role="button" type="submit" class="btn btn-facebook">Create a Quiz</a>
                    <input type="hidden" value="{{ Session::token() }}" name="_token">
                </div>
            </div>
        </div>


        {{--==================================================--}}

        <div id="menu3" class="tab-pane fade">
            <br>
            <div class="row">
                <div class="col-md-12">
                    <input type="text" name="question" id="question" placeholder="Question..." class="form-control">
                    <br>
                    <div class="panel panel-default">
                        <div class="panel panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="text" name="answerOne" id="answerOne" placeholder="Answer #1" class="form-control">
                                    <br>
                                    <input type="text" name="answerTwo" id="answerTwo" placeholder="Answer #2" class="form-control">
                                    <br>
                                    <button type="button" class="btn btn-info pull-right">
                                        <span class="glyphicon glyphicon-plus"></span> Add Answer
                                    </button>
                                    <input type="hidden" value="{{ Session::token() }}" name="_token">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="menu4" class="tab-pane fade">
            <br>
            <div class="row">
                <div class="col-md-8">
                    <button type="submit" class="btn btn-facebook">Create a Quiz</button>
                    <input type="hidden" value="{{ Session::token() }}" name="_token">
                </div>
            </div>
        </div>

        <div id="menu5" class="tab-pane fade">
            <br>
            <div class="row">
                <div class="col-md-8">
                    <button type="submit" class="btn btn-facebook">Create a Quiz</button>
                    <input type="hidden" value="{{ Session::token() }}" name="_token">
                </div>
            </div>
        </div>
    </div>

    <br>


</div>
