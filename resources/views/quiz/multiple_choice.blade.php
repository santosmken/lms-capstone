@extends('layouts.navigation')

@section('content')


    <div class="container">
        <br>
        <div class="row">
            <div class="col-md-12">

                <div class="row ">
                    <form action="{{url('create/quiz', $group->id)}}" method="post">
                        {!! csrf_field() !!}
                        <div class="col-md-9">
                            @include('includes.message-block')
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="text" name="title" class="form-control">
                                        </div>

                                        <div class="col-md-2">
                                            <h5><strong>Time Limit:</strong></h5>
                                        </div>

                                        <div class="col-md-2">
                                            <input type="text" name="time_limit" class="form-control">
                                        </div>

                                        <div class="col-md-2">
                                            <h5>Minutes</h5>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-2">

                                        </div>

                                        <div class="col-md-12">
                                            <h4><strong>Add question to start a quiz..</strong></h4><hr>

                                            <div class="well">
                                                <h5>Type:</h5>

                                                <h5><strong>Question Prompt:</strong></h5>
                                                <div class="row">
                                                    <div class="col-md-9">
                                                        <textarea name="question" id="" cols="40" rows="4" class="form-control"></textarea>
                                                    </div>
                                                </div><br>

                                                <div class="row">
                                                    <div class="col-md-5">
                                                        <h5><strong>1st Response</strong></h5>
                                                        <input type="text" name="first_answer" id="" class="form-control">&nbsp;

                                                        <h5><strong>2nd Response</strong></h5>
                                                        <input type="text" name="second_answer" id="" class="form-control">&nbsp;

                                                        <h5><strong>3rd Response</strong></h5>
                                                        <input type="text" name="third_answer" id="" class="form-control">&nbsp;

                                                        <h5><strong>4th Response</strong></h5>
                                                        <input type="text" name="fourth_answer" id="" class="form-control">&nbsp;
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="radio" style="margin-top: 40px;">
                                                            <label><input type="radio" name="correct_answer">Correct Answer?</label>
                                                        </div>

                                                        <div class="radio" style="margin-top: 70px;">
                                                            <label><input type="radio" name="correct_answer">Correct Answer?</label>
                                                        </div>

                                                        <div class="radio" style="margin-top: 65px;">
                                                            <label><input type="radio" name="correct_answer">Correct Answer?</label>
                                                        </div>

                                                        <div class="radio" style="margin-top: 70px;">
                                                            <label><input type="radio" name="correct_answer">Correct Answer?</label>
                                                        </div>

                                                    </div>

                                                </div>
                                                <button type="submit" class="btn btn-info">Add Response</button>
                                            </div>
                                            END OF FORM
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="col-md-3">
                    <div class="row">
                    <form action="" method="post">
                    <div class="panel panel-default">
                    <div class="panel-heading">
                    <button type="submit" class="btn btn-primary btn-block">Done</button>
                    <br>
                    <div class="row">
                    <div class="col-md-8 col-md-offset-3">
                    <a href="" style="text-decoration: none"><span class="glyphicon glyphicon-print"></span> Print Quiz</a>
                    </div>
                    </div>
                    </div>
                    <div class="panel-body">
                    <hr>
                    <div class="form">
                    <div class="form-group">
                    <label for="about_quiz">About this Quiz</label>
                    <textarea name="quiz_about" id="about_quiz" cols="10" rows="3" class="form-control"></textarea>
                    </div>
                    </div>

                    <hr>
                    <hr>

                    <h5><strong>Quiz Options</strong></h5>

                    <div class="checkbox">
                    <label><input type="checkbox" value=""> Show results</label>
                    </div>

                    <div class="checkbox">
                    <label><input type="checkbox" value=""> Randomize questions</label>
                    </div>

                    </div>
                    </div>

                    </form>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection