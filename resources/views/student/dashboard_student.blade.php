@extends('layouts.master')

@section('title')
    Welcome To Student Page
@endsection

@section('stylesheets')
    <link rel="stylesheet" href="{{asset("css/style.css")}}">
    <link rel="stylesheet" href="{{asset("css/select2.min.css")}}">
@endsection

@section('content')

    @include('student.dashboard_sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <!-- Your Page Content Here -->
            <div class="content">
                <div class="row">
                    <div class="col-md-7">
                        @include('includes.message-block')
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <form action="{{url('home/post')}}" method="post">
                                    {!! csrf_field() !!}
                                    <textarea name="body" cols="30" rows="3" placeholder="Type your note here..." class="form-control"></textarea>
                                    <br>

                                    <select class="form-control select2-multi" name="groups[]" multiple="multiple">
                                        @foreach($user->groups as $group)
                                            <option value="{{$group->id}}">{{$group->group_name}}</option>
                                        @endforeach
                                    </select>
                                    <br><br>

                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="btn-group" role="group">
                                                <a href="" class="btn btn-group"><i class="fa fa-file-text-o"></i></a>
                                                <a href="" class="btn btn-group"><i class="fa fa-chain"></i></a>
                                                <input type="file" name="file">
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="btn-group" role="group">
                                                <button type="reset" class="btn btn-danger">Cancel</button>
                                                <button type="submit" class="btn btn-primary">Send</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-7">
                        <div class="well bg bg-blue-gradient">
                            <h4><strong>Latest Posts</strong></h4>
                        </div>
                        @foreach($user->groups as $group)
                            @foreach($group->posts as $post)
                                <div class="well">
                                    <div class="row">
                                        <div class="col-md-1">
                                            <img src="/uploads/avatars/{{ $post->user->avatar }}" style="width: 50px; height: 50px;">
                                        </div>

                                        <div class="col-md-9 col-md-offset-1">
                                            <p><a href="">{{$post->user->last_name}}</a> to <a href="">{{$group->group_name}}</a></p>
                                            {{$post->body}}
                                            <h6>{{date('D M d,Y | g:i a', strtotime($post->created_at))}}</h6>
                                        </div>

                                        @if(Auth::user()->id == $post->user->id)
                                            <div class="dropdown col-md-1 pull-right">
                                                <a class="dropdown-toggle" type="button" data-toggle="dropdown">
                                                    <span class="caret"></span></a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#"><i class="fa fa-pencil"></i>Edit</a></li>
                                                    <li><a href="{{url('delete/post', $post->id)}}"><i class="fa fa-trash"></i>Delete</a></li>
                                                </ul>
                                            </div>
                                        @else

                                        @endif
                                    </div>
                                    {{--<p>{{$post->id == $group->post_id ? "". $group->group_name : ""}}</p>--}}
                                </div>
                            @endforeach
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->


    @include('sidebar.dashboard_control_sidebar')

    @include('group.partials._group_join')

@endsection

@section('scripts')


    <script type="text/javascript" src="{{asset('js/select2.min.js')}}"></script>
    <script style="text/javascript">

        $('.select2-multi').select2();
        {{--$('.select2-multi').select2().val({!! json_encode($post->groups()->getRelatedIds()) !!}).trigger('change');--}}
         $('.select2-multi').select2({
            placeholder: "Type the name of a group..."

        });

    </script>

@endsection