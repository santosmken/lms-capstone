<div class="tab-content">
    <div id="home" class="tab-pane fade in active">
        <form action="/groups/{{$group->id }}/posts"  method="POST" accept-charset="utf-8" enctype="multipart/form-data">
            <div class="panel panel-default panel-primary">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <textarea name="body" cols="30" rows="3" placeholder="Type your note here..." class="form-control"></textarea>
                            <br>
                            <div class="btn-group" role="group" aria-label="...">
                                <input type="file" name="file" id="">
                                <a href="" class="btn btn-group"><i class="fa fa-file-text-o"></i></a>
                                <a href="" class="btn btn-group"><i class="fa fa-chain"></i></a>
                            </div>

                            <button class="btn btn-primary pull-right" type="submit">Submit</button>
                            <input type="hidden" value="{{ Session::token() }}" name="_token">
                        </div>
                    </div>
                </div>
            </div>
        </form>

        {{--<div class="row posts">--}}
            {{--<div class="col-md-12">--}}
                {{--<div class="panel panel-default">--}}
                    {{--<div class="panel-heading">--}}
                        {{--<h4>Group Posts</h4>--}}
                    {{--</div>--}}
                {{--</div>--}}

                {{--@foreach($group->posts as $post)--}}
                    {{--<div class="panel panel-default">--}}
                        {{--<div class="panel-body">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-md-2">--}}
                                    {{--<img src="/uploads/avatars/{{ $post->user->avatar }}" alt="" class="img-bordered-sm img-responsive" style="height: 70px; width: 70px;">--}}
                                {{--</div>--}}

                                {{--<div class="col-md-10">--}}
                                    {{--<p>{{$post->user->last_name}} to <a href="">{{$group->group_name}}</a></p>--}}
                                    {{--<p>{{$post->body}}</p>--}}
            {{----}}
                                    {{--@if(!empty($post->file_name))--}}

                                        {{--@if(pathinfo($post->file_name, PATHINFO_EXTENSION) == 'docx')--}}
                                            {{--<div class="well" style="background-color: white;">--}}
                                                {{--<div class="media">--}}
                                                    {{--<div class="media-left">--}}
                                                        {{--<i class="fa fa-file-word-o fa-3x" style="color: dodgerblue"></i>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="media-body">--}}
                                                        {{--<a href="{{url('download', $post->id)}}">{{$post->file_name}}</a>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--@elseif(pathinfo($post->file_name, PATHINFO_EXTENSION) == 'pdf')--}}
                                            {{--<div class="well" style="background-color: white;">--}}
                                                {{--<div class="media">--}}
                                                    {{--<div class="media-left">--}}
                                                        {{--<i class="fa fa-file-pdf-o fa-3x" style="color: red"></i>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="media-body">--}}
                                                        {{--<a href="{{url('download', $post->id)}}">{{$post->file_name}}</a>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--@elseif(pathinfo($post->file_name, PATHINFO_EXTENSION) == 'xlsx')--}}
                                            {{--<div class="well" style="background-color: white;">--}}
                                                {{--<div class="media">--}}
                                                    {{--<div class="media-left">--}}
                                                        {{--<i class="fa fa-file-excel-o fa-3x" style="color: green"></i>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="media-body">--}}
                                                        {{--<a href="{{url('download', $post->id)}}">{{$post->file_name}}</a>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--@elseif(pathinfo($post->file_name, PATHINFO_EXTENSION) == 'mp4')--}}
                                            {{--<div class="well" style="background-color: white">--}}
                                                {{--<div class="embed-responsive embed-responsive-16by9">--}}
                                                    {{--<video autoplay loop class="embed-responsive-item" controls>--}}
                                                        {{--<source src="/attachments/{{$post->file_name}}" type="video/mp4">--}}
                                                    {{--</video>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--@else--}}
                                            {{--<div class="well" style="background-color: white;">--}}
                                                {{--<div class="media">--}}
                                                    {{--<div class="media-left">--}}
                                                        {{--<i class="fa fa-file-o" style="color: green"></i>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="media-body">--}}
                                                        {{--<a href="{{url('download', $post->id)}}">{{$post->file_name}}</a>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--@endif--}}
                                    {{--@else--}}
                                    {{--@endif--}}

                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="panel-footer">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-md-4">--}}
                                    {{--@if($post->comments->count() > 0)--}}
                                        {{--<h5 class="text-muted">{{$post->comments->count()}} Replies</h5>--}}
                                    {{--@else--}}
                                        {{--<h5>Reply</h5>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                                {{--<div class="col-md-offset-8">--}}
                                    {{--<h5>{{date("F d Y | g:i a", strtotime($post->created_at))}}</h5>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--@if($post->comments->count() < 0)--}}
                            {{--<div class="panel-footer">--}}
                                {{--@foreach($post->comments as $comment)--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-md-2">--}}
                                            {{--<img src="/uploads/avatars/{{ $comment->user->avatar }}" alt="" style="width: 50px; height: 40px;" class="img img-responsive">--}}
                                        {{--</div>--}}
                                        {{--<div class="col-md-4    ">--}}
                                            {{--<h6>{{$comment->user->first_name .' '.$comment->user->last_name}}</h6>--}}
                                            {{--<h5 style="color: black;">{{$comment->comment}}</h5>--}}
                                            {{--<h6 class="fa fa-circle-o"> {{$comment->created_at->diffForHumans()}}</h6>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<hr>--}}
                                {{--@endforeach--}}
                            {{--</div>--}}
                        {{--@else--}}
                        {{--@endif--}}

                        {{--<div class="panel-footer">--}}
                            {{--<form action="{{url('comment', $post->id)}}" method="post">--}}
                                {{--{!! csrf_field() !!}--}}
                                {{--<div class="form-horizontal">--}}
                                    {{--<div class="form-group">--}}

                                        {{--<div class="col-md-2">--}}
                                            {{--<img src="/uploads/avatars/{{ Auth::user()->avatar }}" alt="" style="width: 50px; height: 40px;" class="img img-thumbnail">--}}
                                        {{--</div>--}}

                                        {{--<div class="col-md-10">--}}
                                            {{--<input type="text" name="comment" class="form-control" placeholder="Type a reply...">--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="form-horizontal">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<div class="col-md-12">--}}
                                            {{--<button type="submit" class="btn btn-primary pull-right">Reply</button>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</form>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--@endforeach--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--Another One--}}
        <div class="row">
            <div class="col-md-12">
                <div class="well bg bg-blue">
                    <div class="row">
                        <div class="col-md-6">
                            <h4><strong>Latest Posts</strong></h4>
                        </div>

                        <div class="col-md-3 pull-right">
                            <div class="dropdown">
                                <a type="button" data-toggle="dropdown" style="font-size: 13px; color: white; text-decoration: none;">Filter posts by
                                    <span class="fa fa-caret-down"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#"><i class="fa fa-pencil-square-o"></i>Latest Posts</a></li>
                                    <li><a href="#"><i class="fa fa-pencil-square-o"></i>Latest Activity</a></li>
                                    <li><a href="#"><i class="fa fa-check-circle-o"></i>Assignments</a></li>
                                    <li><a href="#"><i class="fa fa-question-circle"></i>Quizzes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        @foreach($group->assignments as $assignment)

            <div class="well">
                <div class="row">
                    <div class="col-md-1">
                        <img src="/uploads/avatars/{{ $assignment->user->avatar }}" alt="" style="width: 70px; height: 70px;">
                    </div>

                    <div class="col-md-9 col-md-offset-1">
                        <p><a href="">{{$assignment->user->last_name}}</a> to <a href="">{{$group->group_name}}</a></p>
                        <h5>{{$assignment->title}}</h5>


                        <div class="well">
                            <div class="row">
                                <div class="col-md-2">
                                    <a href="{{route('student.turnedin', $assignment->id)}}" role="button" class="btn btn-default text-primary">
                                        @if($assignment->hasTurnInFromUser($user->id))
                                            Turned In
                                        @else
                                            Turn In
                                        @endif
                                    </a>
                                </div>

                                <div class="col-md-8 col-md-offset-2">
                                    <p>Due Date:  {{$assignment->due_date . " " . date("g:i a", strtotime($assignment->due_time))}}</p>
                                </div>
                            </div>

                            <div class="row">
                                <hr>
                                <div class="col-md-12">
                                    <p>{{$assignment->body}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{--<div class="panel panel-primary">--}}
                {{--<div class="panel-footer">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-md-4">--}}
                            {{--@if($assignment->comments->count() > 0)--}}
                                {{--<h5 class="text-muted">{{$assignment->comments->count()}} Replies</h5>--}}
                            {{--@else--}}
                                {{--<h5>Reply</h5>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                        {{--<div class="col-md-offset-8">--}}
                            {{--<h5>{{date("F d Y | g:i a", strtotime($assignment->created_at))}}</h5>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}

                {{--<div class="panel-footer">--}}
                    {{--@foreach($assignment->comments as $comment)--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-md-2">--}}
                                {{--<img src="/uploads/avatars/{{ $comment->user->avatar }}" alt="" style="width: 50px; height: 40px;" class="img img-thumbnail">--}}
                            {{--</div>--}}
                            {{--<div class="col-md-4    ">--}}
                                {{--<h5>{{$comment->user->first_name .' '.$comment->user->last_name}}</h5>--}}
                            {{--</div>--}}

                            {{--<div class="col-md-5">--}}
                                {{--<h5 style="color: black;">{{$comment->comment}}</h5>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--@endforeach--}}
                {{--</div>--}}

                {{--<div class="panel-footer">--}}
                    {{--<form action="{{url('comment/assignment', $assignment->id)}}" method="post">--}}
                        {{--{!! csrf_field() !!}--}}
                        {{--<div class="form-horizontal">--}}
                            {{--<div class="form-group">--}}
                                {{--<div class="col-md-2">--}}
                                    {{--<img src="/uploads/avatars/{{ Auth::user()->avatar }}" alt="" style="width: 50px; height: 40px;" class="img img-thumbnail">--}}

                                {{--</div>--}}
                                {{--<div class="col-md-10">--}}
                                    {{--<input type="text" name="comment" class="form-control" placeholder="Type a reply...">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-horizontal">--}}
                            {{--<div class="form-group">--}}
                                {{--<div class="col-md-12">--}}
                                    {{--<button type="submit" class="btn btn-primary pull-right">Reply</button>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                    {{--</form>--}}

                {{--</div>--}}
            {{--</div>--}}
        @endforeach
    </div>


    {{----------------------FOLDERS-----------------------}}
    <div id="menu1" class="tab-pane fade">
        <div class="well" style="background-color: white">
            <br>
            <div class="row">
                <div class="col-md-11">
                    <div class="row">
                        <div class="col-md-4">
                            <h4>Folders</h4>
                        </div>
                    </div>

                    <br>

                    <div class="row">
                        <div class="col-md-3 col-md-offset-1">
                            <h6>Name</h6>
                        </div>

                        <div class="pull-right col-md-offset-1">
                            <h6>Modified Date</h6>
                        </div>
                    </div>

                    {{--@foreach($group->folders as $folder)--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-md-4">--}}
                                {{--col-md-offset-1--}}
                                {{--<a href="">{{$folder->folder_title}}</a>--}}
                            {{--</div>--}}

                            {{--<div class="pull-right col-md-offset-3">--}}
                                {{--<p>{{date('M d, Y', strtotime($folder->created_at))}}</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--@endforeach--}}
                    <hr>
                </div>
            </div>
        </div>
    </div>

    {{------------------GROUP MEMBER----------------------}}
    <div id="members" class="tab-pane fade">
        <form action="{{url('home/post')}}"  method="POST" accept-charset="utf-8">
            <div class="panel panel-default panel-primary">
                <div class="panel-body">
                    <h4>Members</h4>
                    <hr>

                    @foreach($group->users as $user)
                        <div class="row">
                            <div class="col-md-2">
                                <img src="/uploads/avatars/{{ $user->avatar }}" alt="" style="width: 70px; height: 70px;">
                            </div>

                            <div class="col-md-5">
                                <h5><strong>{{$user->first_name . ' ' . $user->last_name}}</strong></h5>
                                <h6>{{ucfirst($user->role)}}</h6>
                            </div>
                        </div>
                        <hr>
                    @endforeach
                </div>
            </div>
            <input type="hidden" value="{{ Session::token() }}" name="_token">
        </form>
    </div>


</div>


