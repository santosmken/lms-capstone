@extends('layouts.master')

@section('title')
    Your Groups
@endsection

@section('stylesheets')
    <link rel="stylesheet" href="{{asset("css/style.css")}}">
@endsection

@section('content')
    @extends('student.dashboard_sidebar')

    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">

                <!-- Your Page Content Here -->
                <div class="content">
                    <div class="row">
                        <div class="col-md-8">

                            @include('includes.message-block')

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4>{{$assignment->title}}</h4>
                                    <h5 class="text-muted">{{date('F d Y', strtotime($assignment->due_date))}} @ {{date('g:i a', strtotime($assignment->due_time))}}</h5>
                                </div>
                                <div class="panel-body">
                                    <p>{{$assignment->body}}</p>
                                    @if(empty($assignment->old_file_name))
                                    @else
                                        <div class="well">
                                            <a href="">{{$assignment->old_file_name}}</a>
                                        </div>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>

                    @if(date('Y-m-d g:i a')  >= ( $assignment->due_date . " " . date("g:i a", strtotime($assignment->due_time)) ) && $assignment->lock == 1)
                        <div class="row" id="close">
                            <div class="col-md-8">
                                <div class="panel panel-default">
                                    <div class="panel-footer" style="background-color: white">
                                        <h5><strong>{{$user->first_name . ' ' . $user->last_name}}</strong></h5>
                                        <h5 class="text-muted">Not turned in <span class="label label-danger">LATE</span></h5>
                                        <hr>
                                        <p class="text-info">This assignment is late and has been locked.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @elseif(date('Y-m-d g:i a')  >= ( $assignment->due_date . " " . date("g:i a", strtotime($assignment->due_time)) ) && $assignment->lock == 0)
                        <div class="row" id="close">
                            <div class="col-md-8">
                                <div class="panel panel-default">
                                    <div class="panel-body" style="background-color: white">
                                        <h5><strong>{{$user->first_name . ' ' . $user->last_name}}</strong></h5>
                                        <h5 class="text-muted">Not turned in <span class="label label-danger">LATE</span></h5>
                                        <hr>
                                        <p class="text-info">This assignment is late and has been locked.</p>
                                        <hr>
                                    </div>

                                    <div class="panel-footer">
                                        <h4>Comments</h4>
                                        <textarea name="comment" id="" cols="30" rows="3" placeholder="Type your note here..." class="form-control"></textarea>

                                        <div class="row">
                                            <br>
                                            <form action="" method="post" enctype="multipart/form-data">
                                                <div class="col-md-2">
                                                    <input type="file" name="file" id="">
                                                </div>

                                                <div class="col-md-3 col-md-offset-7 pull-right">
                                                    <button type="submit" class="btn btn-primary">Add Comment</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    @else
                        <form action="/student/{{$assignment->id}}/submission" enctype="multipart/form-data" method="post">
                            {!! csrf_field() !!}

                            <div class="row">
                                <div class="col-md-8">
                                    <div class="panel panel-default">
                                        <div class="panel-body">

                                            <h5><strong>{{$user->first_name . ' ' . $user->last_name}}</strong></h5>

                                            <textarea class="form-control" placeholder="Message to your professor" name="description"></textarea><br>

                                            <input type="file" name="file"><br>

                                            <button type="submit" class="btn btn-primary">Submit</button><hr>

                                            <p class="text-info">This assignment will be locked at {{date('F d Y', strtotime($assignment->due_date)) . " " . date("g:i a", strtotime($assignment->due_time))}}.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                    @endif

                </div>
                <input type="hidden" value="{{ $assignment->id }}" name="assignment_id">

        </section>
    </div>

    @include('sidebar.dashboard_control_sidebar')


@endsection

