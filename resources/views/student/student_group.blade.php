@extends('layouts.master')

@section('title')
    Welcome to the Group
@endsection

@section('stylesheets')

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset("css/select2.min.css")}}">
    <link rel="stylesheet" href="{{asset("css/bootstrap-timepicker.min.css")}}">

@endsection

@section('content')

    @include('student.dashboard_sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">

            <!-- Your Page Content Here -->
            <div class="content">
                <div class="row">

                    <div class="col-md-7">
                        @include('includes.message-block')
                        <div class="panel panel-primary">
                            <div class="panel-heading bg-light-blue">
                                <div class="panel-title">
                                    <h4><strong>{{$group->group_name}}</strong></h4>
                                    <h5>{{$group->user->first_name . ' ' . $group->user->last_name}}</h5>
                                </div>
                            </div>

                            <div class="panel-body">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#home">Posts</a></li>
                                    <li><a data-toggle="tab" href="#menu1">Folders</a></li>
                                    <li><a data-toggle="tab" href="#members">Members <span class="badge">{{$group->users->count()}}</span></a></li>
                                </ul>
                            </div>

                        </div>

                        @include('student.nav-tabs.tabs')

                    </div>
                </div>
            </div>


        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->


    @include('sidebar.dashboard_control_sidebar')
    @include('group.partials._group_join')

@endsection

@section('scripts')

    <script type="text/javascript" src="{{asset('js/select2.min.js')}}"></script>

    <script type="text/javascript">

        $(document).ready(function(){
            $(".nav-tabs a").click(function(){
                $(this).tab('show');
            });
        });

        $('.select2-multi').select2();
        {{--$('.select2-multi').select2().val({!! json_encode($post->groups()->getRelatedIds()) !!}).trigger('change');--}}
         $('.select2-multi').select2({
            placeholder: "Type the name of a group..."
        });

    </script>



@endsection