@extends('layouts.navigation')

@section('title')
    Profile
@endsection

@section('content')
    <br><br>
    <div class="container">
        <div class="well" style="background-color: white;">
           <div class="row">
               <div class="col-md-2">
                   <img src="/uploads/avatars/{{ Auth::user()->avatar }}" alt="" class="img img-responsive img-thumbnail" style="width: 180px; height: 180px;">
               </div>

               <div class="col-md-8">
                   <h3>{{$user->first_name . '. ' . $user->last_name}}</h3>
                   <h5 class="text-muted">{{ucfirst($user->role)}}</h5>
                   <hr>
                   <div class="row">
                       <div class="col-sm-1" style="margin-left: 15px;">
                            <h4><strong>{{$user->posts->count()}}</strong></h4>
                       </div>
                       <div class="col-sm-1 col-sm-offset-1">
                           <h4><strong>{{$user->groups->count()}}</strong></h4>
                       </div>
                   </div>
                   <div class="row">
                       <div class="col-sm-3">
                            <h5>Posts</h5>
                       </div>
                       <div class="col-sm-1 col-sm-pull-1">
                            <h5>Groups</h5>
                       </div>
                   </div>
               </div>
           </div>
        </div>


        <div class="well" style="background-color: white;">
            <h3>Progress</h3>

            <div class="row">
               @foreach($user->groups as $group)
                    <div class="col-md-3">
                        <div class="list-group">
                            <a href="#" class="list-group-item active text-center">{{$group->group_name}}</a>
                            <a href="#" class="list-group-item text-center">0%<br>
                                Grade
                            </a>

                        </div>
                    </div>
               @endforeach
            </div>
        </div>
    </div>


@endsection