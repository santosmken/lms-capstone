@extends('layouts.navigation')

@section('content')
    <br><br>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <ul class="list-group">
                    <li class="list-group-item"><i class="fa fa-pencil-square-o"></i>
                        <a href="/assignment/{{$assignment->id}}/group/{{$group->id}}"><strong> {{$assignment->title}}</strong></a>
                        <p class="text-muted">Due: {{date('M d,Y', strtotime($assignment->due_date))}}</p>
                    </li>
                    <li class="list-group-item">Showing: All</li>
                    <li class="list-group-item">{{$group->group_name}}</li>
                    @foreach($assignment->turnins as $turnin)
                        <li class="list-group-item">
                            @foreach($group->users as $user)
                                <a href="{{url('grading', $turnin->id .'/'.$group->id . '/'. $assignment->id)}}">
                                    <strong>
                                        {{$user->id == $turnin->user_id ? "" .$user->first_name . " " .$user->last_name : ""}}
                                    </strong>
                                </a>
                            @endforeach
                        </li>
                    @endforeach

                                {{--<a href="{{route('grading', $turnin->id)}}">--}}
                </ul>
            </div>

            <div class="col-md-9">
                <div class="well" style="background-color: white;">
                    {{$assignment->title}}
                    <p class="text-muted"><strong>Due:</strong> {{date('M d Y @ g:i A' ,strtotime($assignment->due_date . ' ' .$assignment->due_time))}}</p>

                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#home">Grading Overview</a></li>
                        <li><a href="#menu1">Assignment Detail</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="home" class="tab-pane fade in active">
                            <div class="well">
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-6">
                                        <div class="row">
                                            <div class="col-sm-3 col-sm-offset-1" style="margin-left: 60px;">
                                                <p><strong>{{$assignment->turnins->count()}}</strong></p>
                                            </div>

                                            <div class="col-sm-3" style="padding-left: 45px;">
                                                <p><strong>{{$assignment->turnins->count()}}</strong></p>
                                            </div>

                                            <div class="col-sm-3" style="padding-left: 45px;">
                                                <p><strong>@</strong></p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-4 col-sm-offset-1">
                                                <p class="text-primary">Turned In</p>
                                            </div>

                                            <div class="col-sm-3" style="padding-left: 30px;">
                                                <p class="text-primary">Ungraded</p>
                                            </div>

                                            <div class="col-sm-3" style="padding-left: 30px;">
                                                <p class="text-primary">Graded</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <p><strong>Name</strong></p>
                                </div>

                                <div class="col-md-3 col-md-offset-1">
                                    <p><strong>Latest Submission</strong></p>
                                </div>

                                <div class="col-md-2 col-md-offset-2">
                                    <p><strong>Grade</strong></p>
                                </div>
                            </div>

                            @foreach($assignment->turnins as $turnin)
                                @foreach($users as $user)
                                    <div class="row">
                                        <div class="col-md-4">
                                            {{$user->id == $turnin->user_id ? "" .$user->first_name . " " .$user->last_name : ""}}
                                        </div>
                                        <div class="col-md-4 col-md-offset-1">
                                            {{ $user->id == $turnin->user_id ? "". date('F d Y @ g:i A', strtotime($turnin->created_at)): ""}}
                                        </div>
                                        <div class="col-md-2 col-md-offset-1">
                                            @if($user->id == $turnin->user_id ? "". empty($turnin->grade) :"")
                                                <p style="color:red">0</p>
                                            @else
                                                {{ $user->id == $turnin->user_id ? "". $turnin->grade .' / '. $turnin->total:""}}
                                            @endif
                                        </div>
                                    </div>
                                @endforeach

                                    <hr style="margin-top: 5px;">
                            @endforeach
                        </div>
                        <div id="menu1" class="tab-pane fade">
                            <br>
                            <p>Assigned to: <a href="/group/{{$group->id}}"> {{$group->group_name}}</a></p>
                            <hr>
                            <p>{{$assignment->body}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('sidebar.dashboard_control_sidebar')
@endsection

@section('scripts')
    <script type="text/javascript">

        $(document).ready(function(){
            $(".nav-tabs a").click(function(){
                $(this).tab('show');
            });
        });

    </script>
@endsection


