<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
</head>
<body>

    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <a class="navbar-brand"><i class="glyphicon glyphicon-bullhorn"></i>Vue Events Bulletin Board</a>
        </div>
    </nav>

    <div class="container" id="events">
        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Add an Event</h4>

                </div>

                <div class="panel-body">

                    <div class="form-group">
                        <input class="form-control" placeholder="Event Name" v-model="event.name">
                    </div>
                    
                    <div class="form-group">
                        <textarea class="form-control" placeholder="Event Description" v-model="event.description"></textarea>
                    </div>

                    <div class="form-group">
                        <input type="date" class="form-control" placeholder="Date" v-model="event.date">
                    </div>

                    <button class="btn btn-primary" v-on="click: addEvent">Submit</button>

                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="list-group">

        </div>
    </div>

    <script src="{{asset('js/vue.min.js')}}"></script>
    <script src="{{asset('js/appvue.js')}}"></script>

</body>
</html>